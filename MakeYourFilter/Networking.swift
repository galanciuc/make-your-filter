//
//  Networking.swift
//  MakeYourFilter
//
//  Created by Tolea on 1/15/20.
//  Copyright © 2020 Delta Squad. All rights reserved.
//

import Foundation
import Alamofire

class Networking {
    
    typealias CompletionHandlerFilters = (_ filters: [FilterObject]) -> Void

    let baseURL = "http://192.241.167.9:3000/makeyourfilter/";
    
    init() {
        
    }
    
//    func processFilterInformation(jsonInformation: NSArray) -> [FilterObject] {
//
//        var tempArray : [FilterObject] = [];
//
//        for item in jsonInformation {
//            let itemNSDictionary = item as! NSDictionary
//
//            let name = itemNSDictionary.value(forKey: "selected_filter_name") as! String
//            let value = (itemNSDictionary.value(forKey: "selected_filter_value") as! NSNumber).floatValue
//            let filter_type = itemNSDictionary.value(forKey: "selected_filter_type") as! Int
//            let saturation = (itemNSDictionary.value(forKey: "selected_filter_saturation") as! NSNumber).floatValue
//            let hue = (itemNSDictionary.value(forKey: "selected_filter_hue") as! NSNumber).floatValue
//            let opacity = (itemNSDictionary.value(forKey: "selected_filter_opacity") as! NSNumber).floatValue
//            let intensity = (itemNSDictionary.value(forKey: "filter_intensity") as! NSNumber).floatValue
//
//            let filterObject = FilterObject.init(name: name, type: filter_type, hue: hue, saturation: saturation, value: value, opacity: opacity, intensity: intensity)
//
//            tempArray.append(filterObject)
//        }
//
//        return tempArray
//    }

    func getPlatformFilters(completion: @escaping CompletionHandlerFilters) {
        
        let url = baseURL + "getPlatformFiltersIOS";
        
        AF.request(url, method: .get).responseJSON{ response in
            
            switch response.result {
            case .success(let JSON):
                let response = JSON as! NSDictionary
                
                if (response.value(forKey: "status") as! Int) == 1 {
                    
                    let filterNSArray = response.value(forKey: "filters") as! NSArray
                    
//                    let filterArray = self.processFilterInformation(jsonInformation: filterNSArray)
                    
                    completion([])
                }
                else {
                    completion([])
                }
            default:
                completion([])
            }
        }
    }

}

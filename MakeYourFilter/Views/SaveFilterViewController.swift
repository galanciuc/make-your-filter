//
//  SaveFilterViewController.swift
//  MakeYourFilter
//
//  Created by Tolea on 2/19/20.
//  Copyright © 2020 Delta Squad. All rights reserved.
//

import UIKit
import FirebaseAnalytics

class SaveFilterViewController: UIViewController {
    
    @IBOutlet var firstView: UIView!
    @IBOutlet var secondView: UIView!
    
    @IBOutlet var filterNameTextField: UITextField!
    
    @IBAction func firstViewCancelAction(_ sender: UIButton) {
        guard let parentVC = parent as? EditImageViewController else {
            return
        }
        
        parentVC.popCanceled()
    }
    
    @IBAction func yesAddFilterAction(_ sender: UIButton) {
        Analytics.logEvent("SaveFilter_Tapped", parameters: nil)
        self.firstView.isHidden = true
        self.secondView.isHidden = false
    }
    
    @IBAction func noSavePhotoAction(_ sender: UIButton) {
        Analytics.logEvent("SavePhoto_Tapped", parameters: nil)

        guard let parentVC = parent as? EditImageViewController else {
            return
        }
        
        parentVC.saveImage()
    }
    
    @IBAction func secondViewCancelAction(_ sender: UIButton) {
        self.firstView.isHidden = false
        self.secondView.isHidden = true
    }
    
    @IBAction func saveFilterAction(_ sender: UIButton) {
        Analytics.logEvent("Filter_Saved", parameters: nil)

        if filterNameTextField.text != "" {
            guard let parentVC = parent as? EditImageViewController else {
                return
            }
            
            parentVC.saveFilter(filter_name: filterNameTextField.text!)
        }
        else{
            let alertController = UIAlertController(title: "MakeYourFilter", message:
                "Filter name can not be empty", preferredStyle: .alert)
            alertController.addAction(UIAlertAction(title: "Ok", style: .default))

            self.present(alertController, animated: true, completion: {})
        }
    }
    

    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.firstView.isHidden = false
        self.secondView.isHidden = true
        
        let tap = UITapGestureRecognizer(target: self.view, action: #selector(UIView.endEditing))
        tap.cancelsTouchesInView = false
        self.view.addGestureRecognizer(tap)
    }

}

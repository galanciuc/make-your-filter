//
//  CropViewController.swift
//  MakeYourFilter
//
//  Created by Victoria on 6/30/19.
//  Copyright © 2019 Delta Squad. All rights reserved.
//

import UIKit

class CropViewController: UIViewController {
    
    @IBOutlet var imageView: UIImageView!
    var photo = UIImage()

    override func viewDidLoad() {
        super.viewDidLoad()

    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        imageView.image = photo

    }

    @IBAction func leftButtonAction(_ sender: Any) {
        
    }
    
    @IBAction func rightButtonAction(_ sender: Any) {
        
    }
    
}

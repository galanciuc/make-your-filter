//
//  EditViewController.swift
//  MakeYourFilter
//
//  Created by Victoria on 6/29/19.
//  Copyright © 2019 Delta Squad. All rights reserved.
//

import UIKit

class EditViewController: UIViewController {
    
    @IBOutlet var menuCollectionView: UICollectionView!
    
    let menuNames = ["", "My\n Filters", "Crop", "Light", "Color", "Light\n Leaks", "Effects", "Filters", "Presets"]
    let menuIcons = ["icon", "", "cropIcon", "lightIcon", "colorIcon", "", "effectsIcon", "filtersIcon", "presetsIcon"]
    var photo = UIImage()
    
    @IBOutlet var photoImageView: UIImageView!
    @IBOutlet var topView: UIView!
    
    @IBOutlet var saveShareStackView: UIStackView!
    @IBOutlet var cancelButton: UIButton!
    @IBOutlet var doneButton: UIButton!
    @IBOutlet var backButton: UIButton!
    @IBOutlet var undoButton: UIButton!
    @IBOutlet var redoButton: UIButton!
    @IBOutlet var shareButton: UIButton!
    @IBOutlet var saveButton: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        photoImageView.image = photo
        self.hideToolButtons()
    }
    
    override var prefersStatusBarHidden: Bool {
        return true
    }
    
    func showToolButtons() { // done si cancel
        self.doneButton.isHidden = false
        self.cancelButton.isHidden = false
        self.backButton.isHidden = true
        self.saveShareStackView.isHidden = true
    }
    
    func hideToolButtons() {
        self.doneButton.isHidden = true
        self.cancelButton.isHidden = true
        self.backButton.isHidden = false
        self.saveShareStackView.isHidden = false
    }
    
    func setDefaultView() {
        self.menuCollectionView.isHidden = false
        self.hideToolButtons()
        for subview in self.view.subviews {
            if subview.tag == 100 {
                subview.removeFromSuperview()
            }
        }
    }
    
    
    @IBAction func cancelButtonAction(_ sender: Any) {
        self.setDefaultView()
    }
    

    @IBAction func doneButtonAction(_ sender: Any) {
        self.setDefaultView()
    }
    
}

extension EditViewController: UICollectionViewDelegate, UICollectionViewDataSource {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return menuNames.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = menuCollectionView.dequeueReusableCell(withReuseIdentifier: "cell", for: indexPath) as? MenuCollectionViewCell
        if indexPath.row != 0 {
            cell?.setCircleView()
        }
        if indexPath.row == 0 || indexPath.row == 2 { // Make your filter, crop
            cell?.setViewWithImage()
            cell?.singleIcon.image = UIImage(named: menuIcons[indexPath.row])
        } else if indexPath.row == 1 || indexPath.row == 5 { // My filters, light leaks
            cell?.setViewWithLabel()
            cell?.singleLabel.text = menuNames[indexPath.row]
        } else {
            cell?.setViewWithLabelImage()
            cell?.menuLabel.text = menuNames[indexPath.row]
            cell?.menuIcon.image = UIImage(named: menuIcons[indexPath.row])
        }
        return cell!
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        self.menuCollectionView.isHidden = true
        self.showToolButtons()
        switch indexPath.row {
        case 0: // Make your filter
            let viewController = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "MakeYourFilterVC") as! MakeYourFilterViewController
            viewController.view.tag = 100
            addChildController(content: viewController, frame: self.menuCollectionView.frame)
        case 1: // My filters
            let viewController = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "DefaultVC") as! DefaultViewController
            viewController.view.tag = 100
            viewController.index = indexPath.row
            addChildController(content: viewController, frame: self.menuCollectionView.frame)
        case 2: // Crop
            let viewController = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "CropVC") as! CropViewController
            viewController.view.tag = 100
            viewController.photo = self.photo
            let height = screenHeight - topView.frame.height
            let frame = CGRect(x: 0, y: topView.frame.height, width: screenWidth, height: height)
            addChildController(content: viewController, frame: frame)
        case 3: // Light
            let viewController = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "DefaultSliderVC") as! DefaultSliderViewController
            viewController.index = indexPath.row
            viewController.view.tag = 100
            addChildController(content: viewController, frame: self.menuCollectionView.frame)
        case 4:
            let viewController = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "DefaultSliderVC") as! DefaultSliderViewController
            viewController.index = indexPath.row
            viewController.view.tag = 100
            addChildController(content: viewController, frame: self.menuCollectionView.frame)
        case 5: // Light leaks
            let viewController = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "LightleaksVC") as! LightLeaksViewController
            viewController.view.tag = 100
            addChildController(content: viewController, frame: self.menuCollectionView.frame)
        case 6: // Effects
            let viewController = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "EffectsVC") as! EffectsViewController
            viewController.view.tag = 100
            addChildController(content: viewController, frame: self.menuCollectionView.frame)
        case 7:
            let viewController = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "DefaultVC") as! DefaultViewController
            viewController.view.tag = 100
            viewController.index = indexPath.row
            addChildController(content: viewController, frame: self.menuCollectionView.frame)
        case 8:
            let viewController = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "DefaultVC") as! DefaultViewController
            viewController.view.tag = 100
            viewController.index = indexPath.row
            addChildController(content: viewController, frame: self.menuCollectionView.frame)
            
        default:
            break
        }
        
    }
}

extension EditViewController: UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        var width = 0
        var height = 0
        if indexPath.row == 0 {
            width = 67
            height = 67
        } else {
            width = 50
            height = 50
        }
        return CGSize(width: 72, height: 72)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        return UIEdgeInsets(top: 0, left: 15, bottom: 0, right: 15)
    }
    
    
}

//
//  MakeYourFilterEditViewController.swift
//  MakeYourFilter
//
//  Created by Victoria on 6/30/19.
//  Copyright © 2019 Delta Squad. All rights reserved.
//

import UIKit

class MakeYourFilterEditViewController: UIViewController {
    
    
    @IBOutlet var selectedImage: UIImageView!
    @IBOutlet var selectedLabel: UILabel!
    
    
    @IBOutlet var coloredSlider: CustomSlider!
    @IBOutlet var hueSlider: UISlider!
    @IBOutlet var saturationSlider: UISlider!
    @IBOutlet var valueSlider: UISlider!
    
    var image = UIImage()
    var labelText = String()
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setSlider(slider: hueSlider)
        self.setSlider(slider: valueSlider)
        self.setSlider(slider: saturationSlider)
        self.selectedImage.image = image
        self.selectedLabel.text = labelText

    }
    override func viewDidLayoutSubviews() {
        self.setRadius()
    }
    
    func setRadius() {
        selectedImage.layer.cornerRadius = selectedImage.frame.height / 2
    }
    
    func setSlider(slider: UISlider) {
        slider.maximumValue = 100
        slider.minimumValue = 0
        slider.maximumTrackTintColor = .white
        slider.minimumTrackTintColor = .gray
    }
    

}

//
//  ColorViewController.swift
//  MakeYourFilter
//
//  Created by Tolea on 1/10/20.
//  Copyright © 2020 Delta Squad. All rights reserved.
//

import UIKit

class ColorViewController: UIViewController {
    
    @IBOutlet var temperatureSlider: UISlider!
    @IBOutlet var tintSlider: UISlider!
    @IBOutlet var saturationSlider: UISlider!
    @IBOutlet var vibranceSlider: UISlider!
    @IBOutlet var gammaSlider: UISlider!
    @IBOutlet var hueSlider: UISlider!
    
    @IBOutlet var temperatureLabel: UILabel!
    @IBOutlet var tintLabel: UILabel!
    @IBOutlet var saturationLabel: UILabel!
    @IBOutlet var vibranceLabel: UILabel!
    @IBOutlet var gammaLabel: UILabel!
    @IBOutlet var hueLabel: UILabel!
    
    @IBAction func temperatureStartedChanging(_ sender: UISlider) {
        if let parentVC = parent as? ExploreViewController {
            parentVC.addHistoryUndoSpecificSettings()
        }
        else if let parentVC = parent as? EditImageViewController{
            parentVC.addHistoryUndoSpecificSettings()
        }
    }
    
    @IBAction func tintStartedChanging(_ sender: UISlider) {
        if let parentVC = parent as? ExploreViewController {
            parentVC.addHistoryUndoSpecificSettings()
        }
        else if let parentVC = parent as? EditImageViewController{
            parentVC.addHistoryUndoSpecificSettings()
        }
    }
    
    @IBAction func saturationStartedChanging(_ sender: UISlider) {
        if let parentVC = parent as? ExploreViewController {
            parentVC.addHistoryUndoSpecificSettings()
        }
        else if let parentVC = parent as? EditImageViewController{
            parentVC.addHistoryUndoSpecificSettings()
        }
    }
    
    @IBAction func vibranceStartedChanging(_ sender: UISlider) {
        if let parentVC = parent as? ExploreViewController {
            parentVC.addHistoryUndoSpecificSettings()
        }
        else if let parentVC = parent as? EditImageViewController{
            parentVC.addHistoryUndoSpecificSettings()
        }
    }
    
    @IBAction func gammaStartedChanging(_ sender: UISlider) {
        if let parentVC = parent as? ExploreViewController {
            parentVC.addHistoryUndoSpecificSettings()
        }
        else if let parentVC = parent as? EditImageViewController{
            parentVC.addHistoryUndoSpecificSettings()
        }
    }
    
    @IBAction func hueStartedChanging(_ sender: UISlider) {
        if let parentVC = parent as? ExploreViewController {
            parentVC.addHistoryUndoSpecificSettings()
        }
        else if let parentVC = parent as? EditImageViewController{
            parentVC.addHistoryUndoSpecificSettings()
        }
    }
    
    @IBAction func temperatureChanged(_ sender: UISlider) {
        
        let shownText = getShownTextColor(shownValue: sender.value - 100)
        
        self.temperatureLabel.text = shownText
        
        if let parentVC = parent as? ExploreViewController {
            parentVC.selectedTemperature = (sender.value - 100) / 1250
        }
        else if let parentVC = parent as? EditImageViewController{
            parentVC.selectedTemperature = (sender.value - 100) / 1250
        }
    }
    
    @IBAction func tintChanged(_ sender: UISlider) {
        
        let shownText = getShownTextColor(shownValue: sender.value - 100)
        
        self.tintLabel.text = shownText
        
        if let parentVC = parent as? ExploreViewController {
            parentVC.selectedTint = (sender.value - 100) / 1250
        }
        else if let parentVC = parent as? EditImageViewController{
            parentVC.selectedTint = (sender.value - 100) / 1250
        }
    }
    
    @IBAction func saturationChanged(_ sender: UISlider) {
        
        let shownText = getShownTextColor(shownValue: sender.value - 100)
        
        self.saturationLabel.text = shownText
        
        if let parentVC = parent as? ExploreViewController {
            parentVC.selectedSaturation = (1 + (sender.value - 100) / 300)
        }
        else if let parentVC = parent as? EditImageViewController{
            parentVC.selectedSaturation = (1 + (sender.value - 100) / 300)
        }
    }
    
    @IBAction func vibranceChanged(_ sender: UISlider) {
        
        let shownText = getShownTextColor(shownValue: sender.value * 100)
        
        self.vibranceLabel.text = shownText
        
        if let parentVC = parent as? ExploreViewController {
            parentVC.selectedVibrance = sender.value
        }
        else if let parentVC = parent as? EditImageViewController{
            parentVC.selectedVibrance = sender.value
        }
    }
    
    @IBAction func gammaChanged(_ sender: UISlider) {
        
        let shownText = getShownTextColor(shownValue: (sender.value - 1) * 50)
        
        self.gammaLabel.text = shownText
        
        if let parentVC = parent as? ExploreViewController {
            parentVC.selectedGamma = sender.value
        }
        else if let parentVC = parent as? EditImageViewController{
            parentVC.selectedGamma = sender.value
        }
    }
    
    @IBAction func hueChanged(_ sender: UISlider) {
        
        let shownText = getShownTextColor(shownValue: (sender.value / 6) * 100)
        
        self.hueLabel.text = shownText
        
        if let parentVC = parent as? ExploreViewController {
            parentVC.selectedHue = sender.value
        }
        else if let parentVC = parent as? EditImageViewController{
            parentVC.selectedHue = sender.value
        }
    }
    
    

    override func viewDidLoad() {
        super.viewDidLoad()

        self.setColorSlider(slider: temperatureSlider)
        self.setColorSlider(slider: tintSlider)
        self.setColorSlider(slider: saturationSlider)
        self.setVibranceSlider(slider: vibranceSlider)
        self.setGammaSlider(slider: gammaSlider)
        self.setHueSlider(slider: hueSlider)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        
        self.setupSliders();
    }
    
    func setVibranceSlider(slider: UISlider) {
        slider.maximumValue = 1
        slider.minimumValue = -1
        slider.value = 0
        slider.maximumTrackTintColor = .gray
        slider.setThumbImage(#imageLiteral(resourceName: "thumb_image"), for: UIControl.State.normal)
        slider.minimumTrackTintColor = .white
    }
    
    func setHueSlider(slider: UISlider) {
        slider.maximumValue = 6
        slider.minimumValue = 0
        slider.value = 0
        slider.maximumTrackTintColor = .gray
        slider.setThumbImage(#imageLiteral(resourceName: "thumb_image"), for: UIControl.State.normal)
        slider.minimumTrackTintColor = .white
    }
    
    func setGammaSlider(slider: UISlider) {
        slider.maximumValue = 3
        slider.minimumValue = 0.5
        slider.value = 1
        slider.maximumTrackTintColor = .gray
        slider.setThumbImage(#imageLiteral(resourceName: "thumb_image"), for: UIControl.State.normal)
        slider.minimumTrackTintColor = .white
    }
    
    func setColorSlider(slider: UISlider) {
        slider.maximumValue = 200
        slider.minimumValue = 0
        slider.value = 100
        slider.maximumTrackTintColor = .gray
        slider.setThumbImage(#imageLiteral(resourceName: "thumb_image"), for: UIControl.State.normal)
        slider.minimumTrackTintColor = .white
    }
    
    func getShownTextColor(shownValue: Float) -> String {
           var shownText = "";
           
           if(shownValue > 0) {
               shownText = shownText + "+"
           }
           
           let shownValue2Precisions = round(100 * shownValue) / 100
           
           shownText = shownText + String(shownValue2Precisions)
           
           return shownText
       }
    
    func setupLabels(temperature: Float, tint: Float, saturation: Float, vibrance: Float, gamma: Float, hue: Float) {
        
        let shownTextTemperature = getShownTextColor(shownValue: temperature - 100)
        
        self.temperatureLabel.text = shownTextTemperature
        
        let shownTextTint = getShownTextColor(shownValue: tint - 100)
        
        self.tintLabel.text = shownTextTint
        
        let shownTextSaturation = getShownTextColor(shownValue: saturation - 100)
        
        self.saturationLabel.text = shownTextSaturation
        
        let shownTextVibrance = getShownTextColor(shownValue: vibrance * 100)
        
        self.vibranceLabel.text = shownTextVibrance
        
        let shownTextGamma = getShownTextColor(shownValue: (gamma - 1) * 50)
        
        self.gammaLabel.text = shownTextGamma
        
        let shownTextHue = getShownTextColor(shownValue: (hue / 6) * 100)
        
        self.hueLabel.text = shownTextHue
        
    }
    
    func setupUI(temperature: Float, tint: Float, saturation: Float, vibrance: Float, gamma: Float, hue: Float) {
        
        self.setupLabels(temperature: temperature, tint: tint, saturation: saturation, vibrance: vibrance, gamma: gamma, hue: hue)
        
        temperatureSlider.value = temperature;
        tintSlider.value = tint;
        saturationSlider.value = saturation;
        vibranceSlider.value = vibrance;
        gammaSlider.value = gamma;
        hueSlider.value = hue;
    }
    
    func setupSliders(){
        if let parentVC = parent as? ExploreViewController {
            let temperature = parentVC.selectedTemperature * 1250 + 100;
            let tint = parentVC.selectedTint * 1250 + 100;
            let saturation = (parentVC.selectedSaturation - 1) * 300 + 100;
            let vibrance = parentVC.selectedVibrance
            let gamma = parentVC.selectedGamma
            let hue = parentVC.selectedHue
            
            self.setupUI(temperature: temperature, tint: tint, saturation: saturation, vibrance: vibrance, gamma: gamma, hue: hue)
        }
        else if let parentVC = parent as? EditImageViewController{
            let temperature = parentVC.selectedTemperature * 1250 + 100;
            let tint = parentVC.selectedTint * 1250 + 100;
            let saturation = (parentVC.selectedSaturation - 1) * 300 + 100;
            let vibrance = parentVC.selectedVibrance
            let gamma = parentVC.selectedGamma
            let hue = parentVC.selectedHue
            
            self.setupUI(temperature: temperature, tint: tint, saturation: saturation, vibrance: vibrance, gamma: gamma, hue: hue)
        }
    }

}

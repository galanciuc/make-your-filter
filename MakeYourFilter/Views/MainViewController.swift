//
//  MainViewController.swift
//  MakeYourFilter
//
//  Created by Tolea on 12/30/19.
//  Copyright © 2019 Delta Squad. All rights reserved.
//

import UIKit

class MainViewController: UIViewController, UIImagePickerControllerDelegate, UINavigationControllerDelegate {
    
    let picker = UIImagePickerController()
    
    @IBAction func termsAndConditionsAction(_ sender: Any) {
        self.openTermsAndConditions();
    }
    
    @IBAction func privacyPolicyAction(_ sender: Any) {
        self.openPrivacyPolicy();
    }
    
    @IBAction func exploreAction(_ sender: Any) {
        self.openExplore();
    }
    
    @IBAction func choosePhoto(_ sender: Any) {
        self.openAlert();
    }
    
    func openGallery() {
        self.picker.sourceType = .photoLibrary
        self.present(self.picker, animated: true, completion: nil)
    }
    
    func openCamera() {
        if(UIImagePickerController .isSourceTypeAvailable(.camera)){
            self.picker.sourceType = .camera
            self.present(picker, animated: true, completion: nil)
        } else {
            let alert = UIAlertController(title: "Warning", message: "You do not have camera access.", preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: "OK", style: .default, handler: nil))
            self.present(alert, animated: true)
        }
    }
    
    func openAlert() {
        
        let alertController = UIAlertController(title: nil, message: "Choose Photo", preferredStyle: .actionSheet)
        
        let choosePhotoAction = UIAlertAction(title: "Choose from Camera Roll", style: .default, handler: { (alert: UIAlertAction!) -> Void in
            self.openGallery()
        })
        
        let takePhotoAction = UIAlertAction(title: "Take a Photo", style: .default, handler: { (alert: UIAlertAction!) -> Void in
            self.openCamera()
        })
        
        let cancelAction = UIAlertAction(title: "Cancel", style: .cancel, handler: { (alert: UIAlertAction!) -> Void in
            
        })
        
        alertController.addAction(choosePhotoAction)
        alertController.addAction(takePhotoAction)
        alertController.addAction(cancelAction)
        self.present(alertController, animated: true, completion: nil)
        
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        self.picker.delegate = self
    }
    
    func openExplore() {
        let viewController = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "ExploreVC") as! ExploreViewController
        self.navigationController?.pushViewController(viewController, animated: true);
    }
    
    func openPrivacyPolicy() {
        if let url = URL(string: "https://deltasquad.co/store/privacy.php?n=MakeYourFilter") {
            UIApplication.shared.open(url)
        }
    }
    
    func openTermsAndConditions() {
        if let url = URL(string: "https://deltasquad.co/store/terms.php?n=MakeYourFilter") {
            UIApplication.shared.open(url)
        }
    }
    
    // MARK: - Delegates
    
    func imagePickerController(_ picker: UIImagePickerController,
    didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        guard let selectedImage = info[.originalImage] as? UIImage else {
            fatalError("Expected a dictionary containing an image, but was provided the following: \(info)")
        }
        
        dismiss(animated: true, completion: {});
        
        let viewController = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "EditImageVC") as! EditImageViewController
        
        viewController.image = selectedImage
        
        self.navigationController?.pushViewController(viewController, animated: true)
        
    }
    
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        picker.dismiss(animated: true)
    }
}

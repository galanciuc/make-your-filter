//
// MakeYourFilterViewController MakeYourFilterViewController.swift
//  MakeYourFilter
//
//  Created by Victoria on 6/29/19.
//  Copyright © 2019 Delta Squad. All rights reserved.
//

import UIKit
import SwiftCSV

class MakeYourFilterViewController: UIViewController, UICollectionViewDelegateFlowLayout, UICollectionViewDelegate, UICollectionViewDataSource {
    
    var filterList : [FilterObject] = [];
    
    var selectedCell = -1;
    
    @IBOutlet var opacitySlider: UISlider!
    @IBOutlet var redSlider: UISlider!
    @IBOutlet var greenSlider: UISlider!
    @IBOutlet var blueSlider: UISlider!
    @IBOutlet var intensitySlider: UISlider!
    
    @IBOutlet var redLabel: UILabel!
    @IBOutlet var greenLabel: UILabel!
    @IBOutlet var blueLabel: UILabel!
    @IBOutlet var opacityLabel: UILabel!
    @IBOutlet var intensityLabel: UILabel!
    
    @IBOutlet var filtersButton: UIButton!
    @IBOutlet var blendingModesButton: UIButton!
    @IBOutlet var settingsButton: UIButton!
    
    var hue : Float = 0.0;
    var saturation : Float = 0.0;
    var value : Float = 0.0;
    var opacity : Float = 0.0;

    var filter_type : Int = 0;
    
    var names = ["Normal", "Overlay", "Difference", "Screen", "Multiply", "Dodge", "Exclusion", "Darken", "Soft Light", "Burn", "Lighten", "Hard Light", "Linear Burn", "Addition", "Divide", "Substract", "Vivid Light"]
    @IBOutlet var collectionView: UICollectionView!
    @IBOutlet var filterCollectionView: UICollectionView!
    @IBOutlet var editView: UIView!
    @IBOutlet var imageView: UIImageView!
    @IBOutlet var label: UILabel!
    
    @IBOutlet var sliders: [UISlider]!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        openFilters()
        
        self.setMYFSliders(slider: opacitySlider)
        self.setRedSlider(slider: redSlider)
        self.setGreenSlider(slider: greenSlider)
        self.setBlueSlider(slider: blueSlider)
        self.setIntensitySlider(slider: intensitySlider)
        
        guard let contentsOfUrl = Bundle.main.url(forResource: "filters", withExtension: "csv") else {
            return
        }
        
        do{
        
            let contents = try String(contentsOf: contentsOfUrl)
            
            let csv = try CSV(string: contents)
            
            self.filterList = []
            
            for row in csv.namedRows {
                
                let filter = FilterObject.init(name: row["filter_name"]!, type: Int(row["filter_type"]!)!, red: Float(row["filter_r"]!)!, green: Float(row["filter_g"]!)!, blue: Float(row["filter_b"]!)!, opacity: Float(row["filter_opacity"]!)!, intensity: Float(row["filter_intensity"]!)!)
                
                self.filterList.append(filter)
                
            }
        }
        catch{
            
        }
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        self.imageView.layer.cornerRadius = self.imageView.frame.height / 2
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.setupValues()
        
        self.filterCollectionView.reloadData()
        
        self.startSettingFilterToImage()
    }
    
    let myfFilter = MYFFilter()
    let sharedContext = CIContext(options: [.useSoftwareRenderer : false])
    
    func resizedImage(image: CIImage, scale: CGFloat, aspectRatio: CGFloat) -> UIImage? {

        let filter = CIFilter(name: "CILanczosScaleTransform")
        filter?.setValue(image, forKey: kCIInputImageKey)
        filter?.setValue(scale, forKey: kCIInputScaleKey)
        filter?.setValue(aspectRatio, forKey: kCIInputAspectRatioKey)

        guard let outputCIImage = filter?.outputImage,
            let outputCGImage = sharedContext.createCGImage(outputCIImage,
                                                            from: outputCIImage.extent)
        else {
            return nil
        }

        return UIImage(cgImage: outputCGImage)
    }
    
    func startSettingFilterToImage() {
        
        var image: CIImage?

        if let parentVC = self.parent as? ExploreViewController {
            
            let uiImage = resizedImage(image: parentVC.ciImage!, scale: 0.1, aspectRatio: 1)
            
            image = CIImage(image: uiImage!)
        }
        else if let parentVC = self.parent as? EditImageViewController {
            let uiImage = resizedImage(image: parentVC.ciImage!, scale: 0.1, aspectRatio: 1)
            
            image = CIImage(image: uiImage!)
        }
        
        myfFilter.inputImage = image
        
        DispatchQueue.global(qos: .background).asyncAfter(deadline: .now() + 0.1) {

            for (index, filter) in self.filterList.enumerated() {

                let filteredCIImage = self.myfFilter.outputMYF(filter_type: filter.filter_type, selectedR: filter.filter_r, selectedG: filter.filter_g, selectedB: filter.filter_b, selectedAlpha: filter.filter_opacity)

                let filteredUIImage = UIImage(ciImage: filteredCIImage!)

                filter.setImage(image: filteredUIImage)
                
                DispatchQueue.main.async {
                    var indexPaths: [IndexPath] = []
                    let indexPath = IndexPath.init(row: index, section: 0)
                    indexPaths.append(indexPath)
                    self.filterCollectionView.reloadItems(at: indexPaths)
                }
            }
        }
    }
    
    //MARK: SLIDERS UI
    
    func setMYFSliders(slider: UISlider) {
        slider.maximumValue = 1
        slider.minimumValue = 0
        slider.value = 0
        slider.maximumTrackTintColor = .gray
        slider.setThumbImage(#imageLiteral(resourceName: "thumb_image"), for: UIControl.State.normal)
        slider.minimumTrackTintColor = .white
    }
    
    func setRedSlider(slider: UISlider) {
        slider.maximumValue = 1
        slider.minimumValue = 0
        slider.value = 0
        slider.maximumTrackTintColor = .gray
        slider.setThumbImage(#imageLiteral(resourceName: "thumb_image"), for: UIControl.State.normal)
        slider.minimumTrackTintColor = .red
    }
    
    func setGreenSlider(slider: UISlider) {
        slider.maximumValue = 1
        slider.minimumValue = 0
        slider.value = 0
        slider.maximumTrackTintColor = .gray
        slider.setThumbImage(#imageLiteral(resourceName: "thumb_image"), for: UIControl.State.normal)
        slider.minimumTrackTintColor = .green
    }
    
    func setBlueSlider(slider: UISlider) {
        slider.maximumValue = 1
        slider.minimumValue = 0
        slider.value = 0
        slider.maximumTrackTintColor = .gray
        slider.setThumbImage(#imageLiteral(resourceName: "thumb_image"), for: UIControl.State.normal)
        slider.minimumTrackTintColor = .blue
    }
    
    func setIntensitySlider(slider: UISlider) {
        slider.maximumValue = 1
        slider.minimumValue = 0
        slider.value = 0
        slider.maximumTrackTintColor = .gray
        slider.setThumbImage(#imageLiteral(resourceName: "thumb_image"), for: UIControl.State.normal)
        slider.minimumTrackTintColor = .white
    }
    
    //MARK: EDITING COMMUNICATION
    
    func goBackFromSecondStep() {
        self.collectionView.isHidden = false
        self.editView.isHidden = true
    }
    
    func goToSecondStep() {
        self.collectionView.isHidden = true
        self.editView.isHidden = false
    }
    
    //MARK: BUTTONS ACTIONS
    
    @IBAction func filtersClicked(_ sender: UIButton) {
        if !self.filtersButton.isSelected {
            openFilters()
        }
    }
    
    @IBAction func blendingModesClicked(_ sender: UIButton) {
        if !self.blendingModesButton.isSelected {
            openBlendingModes()
        }
    }
    
    @IBAction func settingsClicked(_ sender: UIButton) {
        if !self.settingsButton.isSelected {
            openSettings()
        }
    }
    
    func openFilters() {
        self.filtersButton.isSelected = true
        self.settingsButton.isSelected = false
        self.blendingModesButton.isSelected = false
        
        self.filterCollectionView.isHidden = false
        self.collectionView.isHidden = true
        self.editView.isHidden = true
    }
    
    func openBlendingModes() {
        self.filtersButton.isSelected = false
        self.settingsButton.isSelected = false
        self.blendingModesButton.isSelected = true
        
        self.editView.isHidden = true
        self.collectionView.isHidden = false
        self.filterCollectionView.isHidden = true
    }
    
    func openSettings() {
        self.filtersButton.isSelected = false
        self.settingsButton.isSelected = true
        self.blendingModesButton.isSelected = false
        
        self.editView.isHidden = false
        self.collectionView.isHidden = true
        self.filterCollectionView.isHidden = true
    }
    
    
    //MARK: SLIDERS ACTIONS
    
    @IBAction func opacityStartChanging(_ sender: UISlider) {
        if let parentVC = parent as? ExploreViewController {
            parentVC.addHistoryUndoSpecificSettings()
        }
        else if let parentVC = parent as? EditImageViewController{
            parentVC.addHistoryUndoSpecificSettings()
        }
    }
    
    @IBAction func redStartChanging(_ sender: UISlider) {
        if let parentVC = parent as? ExploreViewController {
            parentVC.addHistoryUndoSpecificSettings()
        }
        else if let parentVC = parent as? EditImageViewController{
            parentVC.addHistoryUndoSpecificSettings()
        }
    }
    
    @IBAction func greenStartChanging(_ sender: UISlider) {
        if let parentVC = parent as? ExploreViewController {
            parentVC.addHistoryUndoSpecificSettings()
        }
        else if let parentVC = parent as? EditImageViewController{
            parentVC.addHistoryUndoSpecificSettings()
        }
    }
    
    @IBAction func blueStartChanging(_ sender: UISlider) {
        if let parentVC = parent as? ExploreViewController {
            parentVC.addHistoryUndoSpecificSettings()
        }
        else if let parentVC = parent as? EditImageViewController{
            parentVC.addHistoryUndoSpecificSettings()
        }
    }
    
    @IBAction func intensityStartChanging(_ sender: UISlider) {
        if let parentVC = parent as? ExploreViewController {
            parentVC.addHistoryUndoSpecificSettings()
        }
        else if let parentVC = parent as? EditImageViewController {
            parentVC.addHistoryUndoSpecificSettings()
        }
    }
    
    
    @IBAction func opacityChanged(_ sender: UISlider) {
        
        let shownText = getShownText(value: sender.value);
        
        self.opacityLabel.text = shownText;
        
        if let parentVC = parent as? ExploreViewController {
            parentVC.selectedAlpha = sender.value;
        }
        else if let parentVC = parent as? EditImageViewController{
            parentVC.selectedAlpha = sender.value;
        }
    }
    
    @IBAction func intensityChanged(_ sender: UISlider) {
        
        let shownText = getShownText(value: sender.value)
        
        self.intensityLabel.text = shownText;
        
        if let parentVC = parent as? ExploreViewController {
            parentVC.selectedIntensity = sender.value
        }
        else if let parentVC = parent as? EditImageViewController {
            parentVC.selectedIntensity = sender.value;
        }
        
    }
    
    
    @IBAction func redChanged(_ sender: UISlider) {
        
        let shownText = getShownText(value: sender.value);
        
        self.redLabel.text = shownText;
        
        if let parentVC = parent as? ExploreViewController {
            parentVC.selectedR = sender.value;
        }
        else if let parentVC = parent as? EditImageViewController{
            parentVC.selectedR = sender.value;
        }
    }
    
    @IBAction func greenChanged(_ sender: UISlider) {
        
        let shownText = getShownText(value: sender.value);
        
        self.greenLabel.text = shownText;
        
        if let parentVC = parent as? ExploreViewController {
            parentVC.selectedG = sender.value;
        }
        else if let parentVC = parent as? EditImageViewController{
            parentVC.selectedG = sender.value;
        }
    }
    
    @IBAction func blueChanged(_ sender: UISlider) {
        
        let shownText = getShownText(value: sender.value);
        
        self.blueLabel.text = shownText;
        
        if let parentVC = parent as? ExploreViewController {
            parentVC.selectedB = sender.value;
        }
        else if let parentVC = parent as? EditImageViewController{
            parentVC.selectedB = sender.value;
        }
    }
    
    func getShownText(value: Float) -> String {
        let shownValue = value * 100;
        
        var shownText = "";
        
        if(shownValue > 0) {
            shownText = shownText + "+"
        }
        
        let shownValue2Precisions = round(100 * shownValue) / 100
        
        shownText = shownText + String(shownValue2Precisions)
        
        return shownText
    }
    
    func setupSliders(opacity: Float, red: Float, green: Float, blue: Float, intensity: Float) {
        
        self.opacitySlider.value = opacity;
        self.redSlider.value = red;
        self.greenSlider.value = green;
        self.blueSlider.value = blue;
        self.intensitySlider.value = intensity;
        
        self.setupLabels(opacity: opacity, red: red, green: green, blue: blue, intensity: intensity)
        
    }
    
    func setupLabels(opacity: Float, red: Float, green: Float, blue: Float, intensity: Float) {
        
        let shownTextOpacity = getShownText(value: opacity);
        
        self.opacityLabel.text = shownTextOpacity;
        
        let shownTextRed = getShownText(value: red);
        
        self.redLabel.text = shownTextRed;
        
        let shownTextGreen = getShownText(value: green);
        
        self.greenLabel.text = shownTextGreen;
        
        let shownTextBlue = getShownText(value: blue);
        
        self.blueLabel.text = shownTextBlue;
        
        let shownTextIntensity = getShownText(value: intensity)
        
        self.intensityLabel.text = shownTextIntensity
        
    }
    
    func setupValues() {
        if let parentVC = parent as? ExploreViewController {
            let opacity = parentVC.selectedAlpha;
            let red = parentVC.selectedR;
            let green = parentVC.selectedG;
            let blue = parentVC.selectedB;
            let intensity = parentVC.selectedIntensity
            
            self.filter_type = parentVC.selectedFilterType
            self.selectedCell = parentVC.selectedFilter

            self.collectionView.reloadData()
            self.filterCollectionView.reloadData()
            
            self.setupSliders(opacity: opacity, red: red, green: green, blue: blue, intensity: intensity)
        }
        else if let parentVC = parent as? EditImageViewController{
            let opacity = parentVC.selectedAlpha;
            let red = parentVC.selectedR;
            let green = parentVC.selectedG;
            let blue = parentVC.selectedB;
            let intensity = parentVC.selectedIntensity
            
            self.filter_type = parentVC.selectedFilterType
            self.selectedCell = parentVC.selectedFilter

            self.collectionView.reloadData()
            self.filterCollectionView.reloadData()
            
            self.setupSliders(opacity: opacity, red: red, green: green, blue: blue, intensity: intensity)
        }
    }
    
    //MARK: COLLECTION VIEW DELEGATE
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if collectionView == self.collectionView {
            return names.count
        }
        else{
            return filterList.count
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        if collectionView == self.collectionView {
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "cell", for: indexPath) as? MakeYourFilterCollectionViewCell
            cell?.setImageCorner()
            cell?.setLabel(with: names[indexPath.row])
            
            if indexPath.row == filter_type {
                cell?.label.textColor = .white
            }
            else {
                cell?.label.textColor = .gray
            }
            
            return cell!
        }
        else {
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "cell", for: indexPath) as? FilterCollectionViewCell
            DispatchQueue.main.asyncAfter(deadline: .now() + 0.001, execute: {
                cell?.setImageCorner()
                self.view.layoutIfNeeded()
                
                if self.filterList[indexPath.row].filter_image_loaded {
                    cell?.imageView.isHidden = false
                }
            })
            
            if filterList[indexPath.row].filter_image_loaded {
                cell?.activityIndicator.isHidden = true
                cell?.activityIndicator.stopAnimating()
                cell?.imageView.image = filterList[indexPath.row].filter_image
            }
            else{
                cell?.imageView.isHidden = true
                cell?.activityIndicator.isHidden = false
                cell?.activityIndicator.startAnimating()
            }
            
            cell?.setLabel(with: filterList[indexPath.row].filter_name)
            
            if indexPath.row == selectedCell {
                cell?.editView.isHidden = false
            }
            else {
                cell?.editView.isHidden = true
            }

            return cell!
        }
    }
    
    func setupSelectedFilterValues(filter: FilterObject) {
        
        if let parentVC = parent as? ExploreViewController {
            parentVC.selectedR = filter.filter_r
            parentVC.selectedG = filter.filter_g
            parentVC.selectedB = filter.filter_b
            parentVC.selectedFilterType = filter.filter_type;
            parentVC.selectedIntensity = filter.filter_intensity;
            parentVC.selectedAlpha = filter.filter_opacity
        }
        else if let parentVC = parent as? EditImageViewController {
            parentVC.selectedR = filter.filter_r
            parentVC.selectedG = filter.filter_g
            parentVC.selectedB = filter.filter_b
            parentVC.selectedFilterType = filter.filter_type;
            parentVC.selectedIntensity = filter.filter_intensity;
            parentVC.selectedAlpha = filter.filter_opacity
        }
        
        self.setupValues()
        
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        if collectionView == self.collectionView {
        
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "cell", for: indexPath) as? MakeYourFilterCollectionViewCell
            
            self.filter_type = indexPath.row
            
            self.collectionView.reloadData()
            
            if let parentVC = parent as? ExploreViewController {
                parentVC.addHistoryUndoSpecificSettings()
                parentVC.selectNewFilterType(filterType: indexPath.row)
                
                if parentVC.selectedIntensity == 0.0 {
                    parentVC.selectedIntensity = 1.0
                    self.setupValues()
                }
            }
            else if let parentVC = parent as? EditImageViewController{
                parentVC.addHistoryUndoSpecificSettings()
                parentVC.selectNewFilterType(filterType: indexPath.row)
                
                if parentVC.selectedIntensity == 0.0 {
                    parentVC.selectedIntensity = 1.0
                    self.setupValues()
                }
            }
        }
        else {
            if (indexPath.row == selectedCell) {
                self.openSettings()
            }
            else{
                if let parentVC = parent as? ExploreViewController {
                    parentVC.addHistoryUndoSpecificSettings()
                    parentVC.selectedFilter = indexPath.row
                }
                else if let parentVC = parent as? EditImageViewController {
                    parentVC.addHistoryUndoSpecificSettings()
                    parentVC.selectedFilter = indexPath.row
                }
                
                selectedCell = indexPath.row
                
                let indexPaths = collectionView.indexPathsForVisibleItems
                collectionView.reloadItems(at: indexPaths)
                
                self.setupSelectedFilterValues(filter: self.filterList[indexPath.row])
            }
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        if collectionView == self.collectionView {
            return CGSize(width: 65, height: 65)
        }
        else{
            return CGSize(width: 85, height: 85)
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        return UIEdgeInsets(top: 0, left: 15, bottom: 0, right: 15)
    }
    


}

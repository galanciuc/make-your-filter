//
//  LibraryViewController.swift
//  MakeYourFilter
//
//  Created by Victoria on 6/22/19.
//  Copyright © 2019 Delta Squad. All rights reserved.
//

import UIKit

class LibraryViewController: UIViewController, UIImagePickerControllerDelegate, UINavigationControllerDelegate{
    
    
    @IBOutlet var cameraButton: UIButton!
    @IBOutlet var settingsButton: UIButton!
    @IBOutlet var addPhotosButton: UIButton!
    @IBOutlet var noPhotosLabel: UILabel!
    @IBOutlet var photosCollectionView: UICollectionView!
    
    var noPhotosString:String = "Oopsie, you have no photos yet. But we can make it quick. Add photo."
    var noPhotosMutableString = NSMutableAttributedString()
    let picker = UIImagePickerController()
    var photos = [UIImage]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.picker.delegate = self
        if UD.integer(forKey: "imageCount") == 0 {
            self.noPhotosLabel.isHidden = false
            self.photosCollectionView.isHidden = true
            self.setLabel()
        } else {
            self.photosCollectionView.isHidden = false
            self.photosCollectionView.reloadData()
            self.noPhotosLabel.isHidden = true
        }
    }
    
    override var prefersStatusBarHidden: Bool {
        return true
    }
    
    func setLabel() {
        noPhotosMutableString = NSMutableAttributedString(string: noPhotosString, attributes: [NSAttributedString.Key.font:UIFont(name: "Avenir", size: 15.0)!])
        noPhotosMutableString.addAttribute(NSAttributedString.Key.foregroundColor, value: purple, range: NSRange(location:58,length:10))
        noPhotosLabel.attributedText = noPhotosMutableString
    }
    
    func openGallery() {
        self.picker.sourceType = .photoLibrary
        self.present(self.picker, animated: true, completion: nil)
    }
    
    func openCamera() {
        if(UIImagePickerController .isSourceTypeAvailable(.camera)){
            self.picker.sourceType = .camera
            self.present(picker, animated: true, completion: nil)
        } else {
            let alert = UIAlertController(title: "Warning", message: "You do not have camera access.", preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: "OK", style: .default, handler: nil))
            self.present(alert, animated: true)
        }
    }
    

    @IBAction func cameraButtonAction(_ sender: Any) {
//        self.openCamera()
        self.addPhotosButtonAction(UIButton())
        
    }
    
    @IBAction func settingsButtonAction(_ sender: Any) {
       
    }
    
    
    
    @IBAction func addPhotosButtonAction(_ sender: Any) {
        let alertController = UIAlertController(title: nil, message: "Choose Photo", preferredStyle: .actionSheet)
        
        let choosePhotoAction = UIAlertAction(title: "Choose from Camera Roll", style: .default, handler: { (alert: UIAlertAction!) -> Void in
            self.openGallery()
        })
        
        let takePhotoAction = UIAlertAction(title: "Take a Photo", style: .default, handler: { (alert: UIAlertAction!) -> Void in
            self.openCamera()
        })
        
        alertController.addAction(choosePhotoAction)
        alertController.addAction(takePhotoAction)
        self.present(alertController, animated: true, completion: nil)
        
    }
    
    
    // MARK: - Delegates
    
    func imagePickerController(_ picker: UIImagePickerController,
    didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        guard let selectedImage = info[.originalImage] as? UIImage else {
            fatalError("Expected a dictionary containing an image, but was provided the following: \(info)")
        }
        
        photos.append(selectedImage)
        // Save image
        var count = UD.integer(forKey: "imageCount")
        count += 1
        UD.set(count, forKey: "imageCount")
        let data = selectedImage.pngData()!
        do {
            let documentsPath = NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true)[0] as String
            try data.write(to: URL(string: "file://\(documentsPath)/IMG_\(UD.integer(forKey: "imageCount"))")!, options: .atomic)
        } catch {
            print("Error")
        }
        
        dismiss(animated: true, completion: {
            self.noPhotosLabel.isHidden = true
            self.photosCollectionView.isHidden = false
            self.photosCollectionView.reloadData()
        })
    }
    
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        picker.dismiss(animated: true)
    }
}

extension LibraryViewController:
    UICollectionViewDataSource, UICollectionViewDelegate {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return UD.integer(forKey: "imageCount")
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = photosCollectionView.dequeueReusableCell(withReuseIdentifier: "cell", for: indexPath) as? PhotoCollectionViewCell
        do {
            let documentsPath = NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true)[0] as String
            let readData = try Data(contentsOf: URL(string: "file://\(documentsPath)/IMG_\(indexPath.row + 1)")!)
            let retreivedImage = UIImage(data: readData)
            cell!.photoImageView.image = retreivedImage
        } catch {
            print("Error")
        }
        return cell!
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let viewController = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "EditVC") as! EditViewController
        do {
            let documentsPath = NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true)[0] as String
            let readData = try Data(contentsOf: URL(string: "file://\(documentsPath)/IMG_\(indexPath.row + 1)")!)
            let retreivedImage = UIImage(data: readData)
            viewController.photo = retreivedImage!
        } catch {
            print("Error")
        }
        self.navigationController?.pushViewController(viewController, animated: true)
    }
    
    
}

extension LibraryViewController: UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let cellWidth = screenWidth / 3
        let cellHeight = cellWidth * 1.13
        return CGSize(width: cellWidth, height: cellHeight)
    }
    
}



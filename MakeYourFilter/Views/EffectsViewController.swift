//
//  EffectsViewController.swift
//  MakeYourFilter
//
//  Created by Victoria on 6/30/19.
//  Copyright © 2019 Delta Squad. All rights reserved.
//

import UIKit

class EffectsViewController: UIViewController {
    
    @IBOutlet var posterizeSlider: UISlider!
    @IBOutlet var vignettePowerSlider: UISlider!
    @IBOutlet var blurSlider: UISlider!
    
    @IBOutlet var vignettePowerLabel: UILabel!
    @IBOutlet var posterizeLabel: UILabel!
    
    @IBAction func posterizeStartedChanging(_ sender: UISlider) {
        if let parentVC = parent as? ExploreViewController {
            parentVC.addHistoryUndoSpecificSettings()
        }
        else if let parentVC = parent as? EditImageViewController{
            parentVC.addHistoryUndoSpecificSettings()
        }
    }
    
    @IBAction func vignetteStartedChanging(_ sender: UISlider) {
        if let parentVC = parent as? ExploreViewController {
            parentVC.addHistoryUndoSpecificSettings()
        }
        else if let parentVC = parent as? EditImageViewController{
            parentVC.addHistoryUndoSpecificSettings()
        }
    }
    
    @IBAction func blurStartedChanging(_ sender: UISlider) {
        if let parentVC = parent as? ExploreViewController {
            parentVC.addHistoryUndoSpecificSettings()
        }
        else if let parentVC = parent as? EditImageViewController{
            parentVC.addHistoryUndoSpecificSettings()
        }
    }
    
    @IBAction func posterizeChanged(_ sender: UISlider) {
        
        let shownText = getShownTextEffects(shownValue: (sender.value - 1) / 24 * 100)
        
        self.posterizeLabel.text = shownText;
        
        let posterizeValue = 26 - sender.value;
        
        if let parentVC = parent as? ExploreViewController {
            parentVC.selectedPosterize = posterizeValue
        }
        else if let parentVC = parent as? EditImageViewController{
            parentVC.selectedPosterize = posterizeValue
        }
    }
    
    @IBAction func vignettePowerChanged(_ sender: UISlider) {
        
        let shownText = getShownTextEffects(shownValue: sender.value)
               
        self.vignettePowerLabel.text = shownText;
        
        let vignettePowerValue = sender.value * 0.05
        
        if let parentVC = parent as? ExploreViewController {
            parentVC.selectedVignettePower = vignettePowerValue
        }
        else if let parentVC = parent as? EditImageViewController{
            parentVC.selectedVignettePower = vignettePowerValue
        }
    }
    
    @IBAction func blurChanged(_ sender: UISlider) {
        
        if let parentVC = parent as? ExploreViewController {
            parentVC.blurPower = sender.value;
        }
        else if let parentVC = parent as? EditImageViewController{
            parentVC.blurPower = sender.value;
        }
    }
    

    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.setSlider(slider: posterizeSlider);
        self.setVignetteSliders(slider: vignettePowerSlider)
        self.setBlurSlider(slider: blurSlider)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        
        self.setupValues()
    }
    
    func setSlider(slider: UISlider) {
        slider.maximumValue = 25
        slider.minimumValue = 1.0
        slider.value = 1.0
        slider.maximumTrackTintColor = .gray
        slider.setThumbImage(#imageLiteral(resourceName: "thumb_image"), for: UIControl.State.normal)
        slider.minimumTrackTintColor = .white
    }
    
    func setBlurSlider(slider: UISlider) {
        slider.maximumValue = 1.0
        slider.minimumValue = 0.0
        slider.value = 0.5
        slider.maximumTrackTintColor = .gray
        slider.setThumbImage(#imageLiteral(resourceName: "thumb_image"), for: UIControl.State.normal)
        slider.minimumTrackTintColor = .white
    }
    
    func setVignetteSliders(slider: UISlider) {
        slider.maximumValue = 100
        slider.minimumValue = 0.0
        slider.value = 0.0
        slider.maximumTrackTintColor = .gray
        slider.setThumbImage(#imageLiteral(resourceName: "thumb_image"), for: UIControl.State.normal)
        slider.minimumTrackTintColor = .white
    }
    
    func getShownTextEffects(shownValue: Float) -> String {
        var shownText = "";
        
        if(shownValue > 0) {
            shownText = shownText + "+"
        }
        
        let shownValue2Precisions = round(100 * shownValue) / 100
        
        shownText = shownText + String(shownValue2Precisions)
        
        return shownText
    }
    
    func setupLabels(posterize: Float, vignetteDistance: Float, vignettePower: Float) {
        
        let shownTextPosterize = getShownTextEffects(shownValue: (posterize - 1) / 24 * 100)
        
        self.posterizeLabel.text = shownTextPosterize;
        
        let shownTextVignettePower = getShownTextEffects(shownValue: vignettePower)
        
        self.vignettePowerLabel.text = shownTextVignettePower;
    }
    
    func setupUI(posterize: Float, vignetteDistance: Float, vignettePower: Float, blurPower: Float) {
        self.setupLabels(posterize: posterize, vignetteDistance: vignetteDistance, vignettePower: vignettePower)
        
        posterizeSlider.value = posterize;
        vignettePowerSlider.value = vignettePower;
        blurSlider.value = blurPower;
    }

    func setupValues() {
        
        if let parentVC = parent as? ExploreViewController {
            let posterize = 26.0 - parentVC.selectedPosterize;
            let vignetteDistance = (1 - parentVC.selectedVignette) / 0.005;
            let vignettePower = parentVC.selectedVignettePower / 0.05;
            let blurPower = parentVC.blurPower;
            
            self.setupUI(posterize: posterize, vignetteDistance: vignetteDistance, vignettePower: vignettePower, blurPower: blurPower)
        }
        else if let parentVC = parent as? EditImageViewController{
            let posterize = 26.0 - parentVC.selectedPosterize;
            let vignetteDistance = (1 - parentVC.selectedVignette) / 0.005;
            let vignettePower = parentVC.selectedVignettePower / 0.05;
            let blurPower = parentVC.blurPower;
            
            self.setupUI(posterize: posterize, vignetteDistance: vignetteDistance, vignettePower: vignettePower, blurPower: blurPower)
        }
    }
}

//
//  EditImageViewController.swift
//  MakeYourFilter
//
//  Created by Tolea on 1/21/20.
//  Copyright © 2020 Delta Squad. All rights reserved.
//

import UIKit
import MetalKit
import AVFoundation
import FirebaseAnalytics

class EditImageViewController: UIViewController, MTKViewDelegate {
    
    @IBOutlet var menuHolderView: UIView!
    
    var image = UIImage()
    
    @IBOutlet var imageView: UIImageView!
    @IBOutlet var mtkView: MTKView!
    
    @IBOutlet var backArrow: UIImageView!
    @IBOutlet var cancelButton: UIImageView!
    @IBOutlet var saveButton: UIButton!
    @IBOutlet var saveChangesImage: UIImageView!
    @IBOutlet var saveChangesButton: UIButton!
    
    
    var ciImage: CIImage?;
    var finalCiImage: CIImage?;
    
    var context = GraphicContext.init();
       
    var ciContext : CIContext?
    
    let metalFilter = MetalFilter();
    
    var filterSettings = FilterSettings.init(filter_type: 0, r: 0.0, g: 0.0, b: 0.0, alpha: 0.0, brightness: 0.0, contrast: 1.0, shadow: 0.0, highlight: 0.0, lux: 0.0, temperature: 0.0, tint: 0.0, saturation: 1.0, vibrance: 0.0, gamma: 1.0, hue: 0.0, posterize: 25.0, vignette: 1.0, vignette_power: 0.0, blur_power: 0.5, intensity: 0.0, selected_filter: -1)
    
    var filterSettingsBeforeConfigurations = FilterSettings.init(filter_type: 0, r: 0.0, g: 0.0, b: 0.0, alpha: 0.0, brightness: 0.0, contrast: 1.0, shadow: 0.0, highlight: 0.0, lux: 0.0, temperature: 0.0, tint: 0.0, saturation: 1.0, vibrance: 0.0, gamma: 1.0, hue: 0.0, posterize: 25.0, vignette: 1.0, vignette_power: 0.0, blur_power: 0.5, intensity: 0.0, selected_filter: -1)
    
    var historyUndoSettings : [FilterSettings] = []
    var historyRedoSettings : [FilterSettings] = []
    
    var historyUndoSpecificSettings : [FilterSettings] = []
    var historyRedoSpecificSettings : [FilterSettings] = []
    
//    //MARK: MYF FILTER
//
//    let myfFilter = MYFFilter();
//
//    //MARK: LIGHT FILTERS
//    let brightnessFilter = BrightnessFilter();
//    let contrastFilter = ContrastFilter();
//    let shadowsFilter = ShadowsFilter();
//    let luxFilter = LuxFilter();
//
//    //MARK: COLOR FILTERS
//    let temperatureFilter = TemperatureFilter();
//    let saturationFilter = SaturationFilter();
//    let vibranceFilter = VibranceFilter();
//    let gammaFilter = GammaFilter();
//    let hueFilter = HueFilter();
//
//    //MARK: EFFECTS FILTERS
//    let posterizeFilter = PosterizeFilter();
    let vignetteBuiltInFilter = CIFilter(name: "CIVignette");
    let blurFilter = BlurFilter();
    
    //MARK: KERNEL VARIABLES
    var selectedFilterType = 0;
    var selectedR : Float = 0.5;
    var selectedG : Float = 0.5;
    var selectedB : Float = 0.5;
    var selectedAlpha : Float = 0.5;
    var selectedIntensity: Float = 0.0;
    
    var selectedBrightness : Float = 0.0;
    var selectedContrast : Float = 1.0;
    var selectedShadow : Float = 0.0;
    var selectedHighlight : Float = 0.0;
    var selectedLux : Float = 0.0;
    
    var selectedTemperature : Float = 0.0;
    var selectedTint : Float = 0.0;
    var selectedSaturation : Float = 1.0;
    var selectedVibrance : Float = 0.0;
    var selectedGamma : Float = 1.0;
    var selectedHue: Float = 0.0;
    
    var selectedPosterize : Float = 25.0;
    var selectedVignette : Float = 1.0;
    var selectedVignettePower : Float = 0.0;
    var blurPower : Float = 0.5;
    
    var selectedFilter : Int = -1;
    
    var step = 0;
    var openedMenu = 0;

    @IBOutlet weak var menuCollectionView: UICollectionView!
        
        @IBAction func goBackAction(_ sender: Any) {
            self.goBack()
        }
        
        @IBAction func undoAction(_ sender: Any) {
            if step == 0 {
                self.undoSettings()
            }
            else {
                undoSpecificSettings()
            }
        }
        
        @IBAction func redoAction(_ sender: Any) {
            if step == 0 {
                self.redoSettings()
            }
            else {
                redoSpecificSettings()
            }
        }
    
        @IBAction func saveChangesAction(_ sender: UIButton) {
            self.saveChanges()
        }
        
        @IBAction func saveAcction(_ sender: Any) {
            Analytics.logEvent("Save_Tapped", parameters: nil)
            let viewController = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "SaveFilterVC") as! SaveFilterViewController
            viewController.view.tag = 100
            addChildController(content: viewController, frame: self.view.frame)
        }
        
        @IBAction func imageTouchDown(_ sender: UIButton) {
            filterSettings = FilterSettings.init(filter_type: selectedFilterType, r: selectedR, g: selectedG, b: selectedB, alpha: selectedAlpha, brightness: selectedBrightness, contrast: selectedContrast, shadow: selectedShadow, highlight: selectedHighlight, lux: selectedLux, temperature: selectedTemperature, tint: selectedTint, saturation: selectedSaturation, vibrance: selectedVibrance, gamma: selectedGamma, hue: selectedHue, posterize: selectedPosterize, vignette: selectedVignette, vignette_power: selectedVignettePower, blur_power: blurPower, intensity: selectedIntensity, selected_filter: selectedFilter)
            
            selectedFilterType = 0;
            selectedR  = 0.0;
            selectedG = 0.0;
            selectedB = 0.0;
            selectedAlpha = 0.0;
            selectedIntensity = 0.0;
            
            selectedBrightness = 0.0;
            selectedContrast = 1.0;
            selectedShadow = 0.0;
            selectedHighlight = 0.0;
            selectedLux = 0.0;
            
            selectedTemperature = 0.0;
            selectedTint = 0.0;
            selectedSaturation = 1.0;
            selectedVibrance = 0.0;
            selectedGamma = 1.0;
            selectedHue = 0.0;
            
            selectedPosterize = 25.0;
            selectedVignette = 1.0;
            selectedVignettePower = 0.0;
            blurPower = 0.5;
        }
        
        @IBAction func imageTouchUp(_ sender: UIButton) {
            setupFilterSettings(filter_settings: filterSettings)
        }
        
//        let menuNames = ["", "Light", "Color", "Effects", "Filters", "Presets"]
//        let menuIcons = ["icon", "lightIcon", "colorIcon", "effectsIcon", "filtersIcon", "presetsIcon"]
    
        let menuNames = ["Make Your\nFilter", "My\nFilters", "Light", "Color", "Effects"]
        let menuIcons = ["icon", "", "lightIcon", "colorIcon", "effectsIcon"]
        
        override func viewDidLoad() {
            super.viewDidLoad()
            
//            imageView.image = image;
            
            self.checkMyFilters()
            
            self.context = GraphicContext.init();
            
            self.ciContext = CIContext(mtlDevice: context.mDevice!)
            
            self.mtkView.device = context.mDevice!
            self.mtkView.framebufferOnly = false
            self.mtkView.delegate = self
            
            ciImage = CIImage(image: image, options: [.applyOrientationProperty:true])?.oriented(forExifOrientation: imageOrientationToTiffOrientation(value: image.imageOrientation));
            
            Analytics.logEvent("EditPage_Open", parameters: nil)
            print("test")
        }
    
    func popCanceled() {
        removeChildController(content: children.last!)
    }
    
    func saveFilter(filter_name: String) {
        if isKeyPresentInUserDefaults(key: "my_filters") {
            let userDefaults = UserDefaults.standard
            
            do{
            
            let decoded  = userDefaults.data(forKey: "my_filters")
                
                var decodedFilters = try NSKeyedUnarchiver.unarchiveTopLevelObjectWithData(decoded!) as! [MyFilterObject]
            
            let newFilter = MyFilterObject.init(name: filter_name, type: selectedFilterType, red: selectedR, green: selectedG, blue: selectedB, opacity: selectedAlpha)
            
            decodedFilters.append(newFilter)
            
                let encodedData: Data = try NSKeyedArchiver.archivedData(withRootObject: decodedFilters, requiringSecureCoding: false)
                userDefaults.set(encodedData, forKey: "my_filters")
                userDefaults.synchronize()
                
                 self.saveImage();
                }
            catch {
                
            }
        }
    }
    
    func saveImage() {
        removeChildController(content: children.last!)
        
        let optionMenu = UIAlertController(title: nil, message: "Save Photo", preferredStyle: .actionSheet)

        let saveAction = UIAlertAction(title: "Save to Camera Roll", style: .default, handler:
        {
            (alert: UIAlertAction!) -> Void in

            let cgImage : CGImage = self.ciContext!.createCGImage(self.finalCiImage!, from: self.finalCiImage!.extent)!

            let image = UIImage(cgImage: cgImage)
            self.writeToPhotoAlbum(image: image)

        })

        let shareAction = UIAlertAction(title: "Share", style: .default, handler:
        {
            (alert: UIAlertAction!) -> Void in

            let cgImage : CGImage = self.ciContext!.createCGImage(self.finalCiImage!, from: self.finalCiImage!.extent)!

            let image = UIImage(cgImage: cgImage)

            let imageToShare = [ image ]

            let activityViewController = UIActivityViewController(activityItems: imageToShare, applicationActivities: [])
            activityViewController.popoverPresentationController?.sourceView = self.view // so that iPads won't crash

            // exclude some activity types from the list (optional)
            activityViewController.excludedActivityTypes = []

            // present the view controller
            self.present(activityViewController, animated: true, completion: nil)
        })

        let cancelAction = UIAlertAction(title: "Cancel", style: .cancel, handler:
        {
            (alert: UIAlertAction!) -> Void in

        })

        optionMenu.addAction(saveAction)
//            optionMenu.addAction(shareAction)
        optionMenu.addAction(cancelAction)

        self.present(optionMenu, animated: true, completion: nil)
    }
    
    func saveChanges() {
        step = 0;
            
        let childVC = children.first;
        
        removeChildController(content: childVC!)
    
        self.menuCollectionView.isHidden = false;
        
        self.backArrow.isHidden = false
        self.cancelButton.isHidden = true
        
        self.saveButton.isHidden = false
        self.saveButton.isEnabled = true
        self.saveChangesImage.isHidden = true
        self.saveChangesButton.isEnabled = false
        self.saveChangesButton.isHidden = true
        
        self.historyRedoSettings = []
        self.historyUndoSpecificSettings = []
        self.historyRedoSpecificSettings = []
    }
    
    func openMakeYourFilter() {
        
        let childVC = children.first;
        
        removeChildController(content: childVC!)
        
        let viewController = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "MakeYourFilterVC") as! MakeYourFilterViewController
        viewController.view.tag = 100
        addChildController(content: viewController, frame: self.menuHolderView.frame)
        
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.01) {
            viewController.openSettings()
        }
        
    }
        
        func goBack() {
            
            if(step == 0){
                self.navigationController?.popViewController(animated: true);
            }
            else if(step == 1){
                step = 0;
                
                let childVC = children.first;
                
                removeChildController(content: childVC!)
            
                self.menuCollectionView.isHidden = false;
                
                self.backArrow.isHidden = false
                self.cancelButton.isHidden = true
                self.saveButton.isHidden = false;
                self.saveButton.isEnabled = true
                self.saveChangesImage.isHidden = true
                self.saveChangesButton.isHidden = true
                self.saveChangesButton.isEnabled = false
                
                resetSettings()
            }
            else if(step == 2){
                step = 1;
                
                guard let childVC = children.first as? MakeYourFilterViewController else{
                    return;
                }
                
                self.backArrow.isHidden = true
                self.cancelButton.isHidden = false

                childVC.goBackFromSecondStep();
            }
        }
    
    func resetSettings() {
        historyUndoSettings.popLast()
        
        setupFilterSettings(filter_settings: filterSettingsBeforeConfigurations)
    }
    
    func addHistoryUndoSpecificSettings() {
        let currentFilterSettings = FilterSettings.init(filter_type: selectedFilterType, r: selectedR, g: selectedG, b: selectedB, alpha: selectedAlpha, brightness: selectedBrightness, contrast: selectedContrast, shadow: selectedShadow, highlight: selectedHighlight, lux: selectedLux, temperature: selectedTemperature, tint: selectedTint, saturation: selectedSaturation, vibrance: selectedVibrance, gamma: selectedGamma, hue: selectedHue, posterize: selectedPosterize, vignette: selectedVignette, vignette_power: selectedVignettePower, blur_power: blurPower, intensity: selectedIntensity, selected_filter: selectedFilter)
        
        historyUndoSpecificSettings.append(currentFilterSettings)
    }
    
    func setupFilterSettings(filter_settings: FilterSettings) {
        selectedFilterType = filter_settings.selectedFilterType;
        selectedR  = filter_settings.selectedR;
        selectedG = filter_settings.selectedG;
        selectedB = filter_settings.selectedB;
        selectedAlpha = filter_settings.selectedAlpha;
        selectedIntensity = filter_settings.selectedIntensity
        
        selectedBrightness = filter_settings.selectedBrightness;
        selectedContrast = filter_settings.selectedContrast;
        selectedShadow = filter_settings.selectedShadow;
        selectedHighlight = filter_settings.selectedHighlight;
        selectedLux = filter_settings.selectedLux;
        
        selectedTemperature = filter_settings.selectedTemperature;
        selectedTint = filter_settings.selectedTint;
        selectedSaturation = filter_settings.selectedSaturation;
        selectedVibrance = filter_settings.selectedVibrance;
        selectedGamma = filter_settings.selectedGamma;
        selectedHue = filter_settings.selectedHue;
        
        selectedPosterize = filter_settings.selectedPosterize;
        selectedVignette = filter_settings.selectedVignette;
        selectedVignettePower = filter_settings.selectedVignettePower;
        blurPower = filter_settings.blurPower;
        
        selectedFilter = filter_settings.selectedFilter;
    }
    
    func updateChildSliders() {
        if let childVC = children.first as? DefaultSliderViewController {
            childVC.setupSliders()
        }
        else if let childVC = children.first as? ColorViewController {
            childVC.setupSliders()
        }
        else if let childVC = children.first as? EffectsViewController {
            childVC.setupValues()
        }
        else if let childVC = children.first as? MakeYourFilterViewController {
            childVC.setupValues()
        }
    }
    
    func undoSpecificSettings() {
        if self.historyUndoSpecificSettings.count != 0 {
            
            let currentFilterSettings = FilterSettings.init(filter_type: selectedFilterType, r: selectedR, g: selectedG, b: selectedB, alpha: selectedAlpha, brightness: selectedBrightness, contrast: selectedContrast, shadow: selectedShadow, highlight: selectedHighlight, lux: selectedLux, temperature: selectedTemperature, tint: selectedTint, saturation: selectedSaturation, vibrance: selectedVibrance, gamma: selectedGamma, hue: selectedHue, posterize: selectedPosterize, vignette: selectedVignette, vignette_power: selectedVignettePower, blur_power: blurPower, intensity: selectedIntensity, selected_filter: selectedFilter)
            
            historyRedoSpecificSettings.append(currentFilterSettings)
            
            let previousSettings = historyUndoSpecificSettings[historyUndoSpecificSettings.count - 1]
            
            historyUndoSpecificSettings.popLast()
            
            setupFilterSettings(filter_settings: previousSettings)
            
            updateChildSliders()
        }
    }
    
    func redoSpecificSettings() {
        if self.historyRedoSpecificSettings.count != 0 {
            
            let currentFilterSettings = FilterSettings.init(filter_type: selectedFilterType, r: selectedR, g: selectedG, b: selectedB, alpha: selectedAlpha, brightness: selectedBrightness, contrast: selectedContrast, shadow: selectedShadow, highlight: selectedHighlight, lux: selectedLux, temperature: selectedTemperature, tint: selectedTint, saturation: selectedSaturation, vibrance: selectedVibrance, gamma: selectedGamma, hue: selectedHue, posterize: selectedPosterize, vignette: selectedVignette, vignette_power: selectedVignettePower, blur_power: blurPower, intensity: selectedIntensity, selected_filter: selectedFilter)
            
            historyUndoSpecificSettings.append(currentFilterSettings)
            
            let previousSettings = historyRedoSpecificSettings[historyRedoSpecificSettings.count - 1]
            
            historyRedoSpecificSettings.popLast()
            
            setupFilterSettings(filter_settings: previousSettings)
            
            updateChildSliders()
        }
    }
    
    func redoSettings() {
        if self.historyRedoSettings.count != 0 {
            
            let currentFilterSettings = FilterSettings.init(filter_type: selectedFilterType, r: selectedR, g: selectedG, b: selectedB, alpha: selectedAlpha, brightness: selectedBrightness, contrast: selectedContrast, shadow: selectedShadow, highlight: selectedHighlight, lux: selectedLux, temperature: selectedTemperature, tint: selectedTint, saturation: selectedSaturation, vibrance: selectedVibrance, gamma: selectedGamma, hue: selectedHue, posterize: selectedPosterize, vignette: selectedVignette, vignette_power: selectedVignettePower, blur_power: blurPower, intensity: selectedIntensity, selected_filter: selectedFilter)
            
            historyUndoSettings.append(currentFilterSettings)
            
            let previousSettings = historyRedoSettings[historyRedoSettings.count - 1]
            
            historyRedoSettings.popLast()
            
            setupFilterSettings(filter_settings: previousSettings)
            
        }
    }
    
    func undoSettings() {
        
        if self.historyUndoSettings.count != 0 {
            
            let currentFilterSettings = FilterSettings.init(filter_type: selectedFilterType, r: selectedR, g: selectedG, b: selectedB, alpha: selectedAlpha, brightness: selectedBrightness, contrast: selectedContrast, shadow: selectedShadow, highlight: selectedHighlight, lux: selectedLux, temperature: selectedTemperature, tint: selectedTint, saturation: selectedSaturation, vibrance: selectedVibrance, gamma: selectedGamma, hue: selectedHue, posterize: selectedPosterize, vignette: selectedVignette, vignette_power: selectedVignettePower, blur_power: blurPower, intensity: selectedIntensity, selected_filter: selectedFilter)
            
            historyRedoSettings.append(currentFilterSettings)
            
            let previousSettings = historyUndoSettings[historyUndoSettings.count - 1]
            
            historyUndoSettings.popLast()
            
            setupFilterSettings(filter_settings: previousSettings)
            
        }
    }
        
        func goToMakeYourPhotosGreatAgain() {
            let viewController = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "MYPGAVC") as! MakeYourPhotosGreatAgainViewController
            self.navigationController?.pushViewController(viewController, animated: true);
        }
        
        func selectNewFilterType(filterType: Int){
            self.selectedFilterType = filterType;
        }
    
    func writeToPhotoAlbum(image: UIImage) {
        UIImageWriteToSavedPhotosAlbum(image, self, #selector(saveError), nil)
    }
    
    @objc func saveError(_ image: UIImage, didFinishSavingWithError error: Error?, contextInfo: UnsafeRawPointer) {
        
        if let error = error {
            //TODO ALERT ERROR
        }
        else {
            let alertController = UIAlertController(title: "MakeYourFilter", message:
                "Image saved successfully to phone library", preferredStyle: .alert)
            alertController.addAction(UIAlertAction(title: "Ok", style: .default))

            self.present(alertController, animated: true, completion: {})
        }
        
    }
    
    func draw(in view: MTKView) {
        
        self.blurFilter.inputImage = self.ciImage;

        let blurImage = self.blurFilter.outputBlur(blurPower: self.blurPower);
        
        self.metalFilter.inputImage = blurImage;
        
        let metalImage = self.metalFilter.outputImage(filter_type: self.selectedFilterType, selectedR: self.selectedR, selectedG: self.selectedG, selectedB: self.selectedB, selectedA: self.selectedAlpha, contrast: self.selectedContrast, brightness: self.selectedBrightness, shadows: self.selectedShadow, highlights: self.selectedHighlight, lux: self.selectedLux, temperature: self.selectedTemperature, tint: self.selectedTint, saturation: self.selectedSaturation, vibrance: self.selectedVibrance, gamma: self.selectedGamma, hue: self.selectedHue, posterize: self.selectedPosterize, intensity: self.selectedIntensity)
        
        self.vignetteBuiltInFilter?.setValue(metalImage, forKey: kCIInputImageKey);
        self.vignetteBuiltInFilter?.setValue(self.selectedVignette, forKey: kCIInputRadiusKey);
        self.vignetteBuiltInFilter?.setValue(self.selectedVignettePower, forKey: kCIInputIntensityKey);

        if let vignetteOutput = self.vignetteBuiltInFilter?.value(forKey: kCIOutputImageKey) as? CIImage {
            
            self.finalCiImage = vignetteOutput
            
            view.currentDrawable?.layer.isOpaque = false
            
            var size = mtkView.bounds
            size.size = mtkView.drawableSize
            size = AVMakeRect(aspectRatio: finalCiImage!.extent.size, insideRect: size)
            let filteredImage = finalCiImage!.transformed(by: CGAffineTransform(
                scaleX: size.size.width/finalCiImage!.extent.size.width,
                y: size.size.height/finalCiImage!.extent.size.height))
            let x = -size.origin.x
            let y = -size.origin.y
            
            let commanQueue = context.mDevice!.makeCommandQueue()
            
            let buffer = commanQueue!.makeCommandBuffer()!
            ciContext!.render(filteredImage,
                                   to: view.currentDrawable!.texture,
                                   commandBuffer: buffer,
                                   bounds: CGRect(origin:CGPoint(x:x, y:y), size:view.drawableSize),
                                   colorSpace: CGColorSpaceCreateDeviceRGB())
            buffer.present(view.currentDrawable!)
            buffer.commit()
        }
        
    }
    
    func isKeyPresentInUserDefaults(key: String) -> Bool {
        return UserDefaults.standard.object(forKey: key) != nil
    }
    
    func checkMyFilters() {
        if !isKeyPresentInUserDefaults(key: "my_filters") {
            let filters : [MyFilterObject] = []
            
            let userDefaults = UserDefaults.standard
            
            do{
                let encodedData: Data = try NSKeyedArchiver.archivedData(withRootObject: filters, requiringSecureCoding: false)
                userDefaults.set(encodedData, forKey: "my_filters")
                userDefaults.synchronize()
                }
            catch {
                
            }
        }
    }
    
    func mtkView(_ view: MTKView, drawableSizeWillChange size: CGSize) {
        
    }
    
    func imageOrientationToTiffOrientation(value: UIImage.Orientation) -> Int32
    {
        switch (value)
        {
        case .up:
            return 1
        case .down:
            return 3
        case .left:
            return 8
        case .right:
            return 6
        case .upMirrored:
            return 2
        case .downMirrored:
            return 4
        case .leftMirrored:
            return 5
        case .rightMirrored:
            return 7
        @unknown default:
            fatalError("ERROR")
        }
    }
        
}


    extension EditImageViewController: UICollectionViewDelegate, UICollectionViewDataSource {
        func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
            return menuNames.count
        }
        
        func enableConfigurationButtons() {
            self.backArrow.isHidden = true
            self.cancelButton.isHidden = false
            self.saveButton.isHidden = true
            self.saveButton.isEnabled = false
            self.saveChangesImage.isHidden = false
            self.saveChangesButton.isHidden = false
            self.saveChangesButton.isEnabled = true
            
            self.filterSettingsBeforeConfigurations = FilterSettings.init(filter_type: selectedFilterType, r: selectedR, g: selectedG, b: selectedB, alpha: selectedAlpha, brightness: selectedBrightness, contrast: selectedContrast, shadow: selectedShadow, highlight: selectedHighlight, lux: selectedLux, temperature: selectedTemperature, tint: selectedTint, saturation: selectedSaturation, vibrance: selectedVibrance, gamma: selectedGamma, hue: selectedHue, posterize: selectedPosterize, vignette: selectedVignette, vignette_power: selectedVignettePower, blur_power: blurPower, intensity: selectedIntensity, selected_filter: selectedFilter)
            
        self.historyUndoSettings.append(self.filterSettingsBeforeConfigurations)
            
            self.historyUndoSpecificSettings = []
            self.historyRedoSpecificSettings = []
        }
        
        func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
            let cell = menuCollectionView.dequeueReusableCell(withReuseIdentifier: "cell", for: indexPath) as? MenuCollectionViewCell
//            if indexPath.row != 0 {
                DispatchQueue.main.asyncAfter(deadline: .now() + 0.1, execute: {
                    cell?.setCircleView()
                })
//            }
            if indexPath.row == 1 { // My Filters
                cell?.setViewWithLabel()
                cell?.singleLabel.text = menuNames[indexPath.row]
            } else {
                cell?.setViewWithLabelImage()
                cell?.menuLabel.text = menuNames[indexPath.row]
                cell?.menuIcon.image = UIImage(named: menuIcons[indexPath.row])
            }
            return cell!
        }
        
        func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
    //        self.showToolButtons()

            switch indexPath.row {
            case 0: // Make your filter
            Analytics.logEvent("MYF_Open", parameters: nil)

            self.menuCollectionView.isHidden = true
            
            step = 1;
            openedMenu = 0;
            
            enableConfigurationButtons()
            
                let viewController = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "MakeYourFilterVC") as! MakeYourFilterViewController
                viewController.view.tag = 100
                addChildController(content: viewController, frame: self.menuHolderView.frame)
            case 1:
                Analytics.logEvent("MyFilters_Open", parameters: nil)

                self.menuCollectionView.isHidden = true
                
                step = 1;
                openedMenu = 0;
                
                enableConfigurationButtons()
                
                    let viewController = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "MyFiltersVC") as! MyFiltersViewController
                    viewController.view.tag = 100
                    addChildController(content: viewController, frame: self.menuHolderView.frame)
            case 2: // Light
                Analytics.logEvent("Light_Open", parameters: nil)

                self.menuCollectionView.isHidden = true
                
                step = 1;
                openedMenu = 1;

                enableConfigurationButtons()
                
                let viewController = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "DefaultSliderVC") as! DefaultSliderViewController
                viewController.index = indexPath.row
                viewController.view.tag = 100
                addChildController(content: viewController, frame: self.menuHolderView.frame)
            case 3:
                Analytics.logEvent("Color_Open", parameters: nil)

                self.menuCollectionView.isHidden = true
                
                step = 1;
                openedMenu = 2;

                enableConfigurationButtons()
                
                let viewController = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "ColorVC") as! ColorViewController
                viewController.view.tag = 100
                addChildController(content: viewController, frame: self.menuHolderView.frame)
            case 4: // Effects
                Analytics.logEvent("Effects_Open", parameters: nil)

                self.menuCollectionView.isHidden = true
                
                step = 1;
                openedMenu = 3;

                enableConfigurationButtons()
                
                let viewController = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "EffectsVC") as! EffectsViewController
                viewController.view.tag = 100
                addChildController(content: viewController, frame: self.menuHolderView.frame)
            case 5:
                self.menuCollectionView.isHidden = true
                
                step = 1;
                openedMenu = 4;

                enableConfigurationButtons()
                
                let viewController = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "PlatformFitersVC") as! FiltersViewController
                viewController.view.tag = 100
                addChildController(content: viewController, frame: self.menuHolderView.frame)
            case 6:
                goToMakeYourPhotosGreatAgain()
            default:
                break
            }
        }
    }

    extension EditImageViewController: UICollectionViewDelegateFlowLayout {
        
        func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
            return CGSize(width: 72, height: 72)
        }
        
        func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {

            return UIEdgeInsets(top: 0, left: 10, bottom: 0, right: 10)
        }
        
        
    }

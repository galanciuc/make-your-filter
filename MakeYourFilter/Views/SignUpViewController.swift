//
//  SignUpViewController.swift
//  MakeYourFilter
//
//  Created by Tolea on 1/2/20.
//  Copyright © 2020 Delta Squad. All rights reserved.
//

import UIKit

class SignUpViewController: UIViewController {
    
    @IBOutlet weak var emailField: UITextField!
    @IBOutlet weak var passwordField: UITextField!
    @IBOutlet weak var repeatPasswordField: UITextField!
    @IBOutlet weak var showPasswordButton: UIButton!
    @IBOutlet weak var showRepeatPasswordButton: UIButton!
    
    var secureTextEntry = true;
    var secureTextEntryRepeat = true;
    
    @IBAction func goBack(_ sender: Any) {
        self.goBack();
    }
    
    @IBAction func openSignInAction(_ sender: Any) {
        self.openSignIn();
    }
    
    @IBAction func showPasswordAction(_ sender: Any) {
        self.showPassword();
    }
    
    @IBAction func showRepeatPasswordAction(_ sender: Any) {
        self.showRepeatPassword();
    }
    
    @IBAction func termsAndConditionsAction(_ sender: Any) {
        self.openTermsAndConditions();
    }
    
    @IBAction func privacyPolicyAction(_ sender: Any) {
        self.openPrivacyPolicy();
    }
    
    @IBAction func signUpAction(_ sender: Any) {
        self.trySignUp();
    }

    override func viewDidLoad() {
        super.viewDidLoad();
    }
    
    func goBack() {
        self.navigationController?.popViewController(animated: true);
    }
    
    func openSignIn() {
        let viewController = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "SignInVC") as! SignInViewController
        self.navigationController?.pushViewController(viewController, animated: true);
    }
    
    func showPassword() {
        secureTextEntry = !secureTextEntry
        passwordField.isSecureTextEntry = secureTextEntry
        showPasswordButton.setTitle(secureTextEntry ? "show" : "hide", for: UIControl.State.normal)
    }
    
    func showRepeatPassword() {
        secureTextEntryRepeat = !secureTextEntryRepeat
        repeatPasswordField.isSecureTextEntry = secureTextEntryRepeat
        showRepeatPasswordButton.setTitle(secureTextEntryRepeat ? "show" : "hide", for: UIControl.State.normal)
    }
    
    func openTermsAndConditions() {
        if let url = URL(string: "https://deltasquad.co/store/terms.php?n=MakeYourFilter") {
            UIApplication.shared.open(url)
        }
    }
    
    func openPrivacyPolicy() {
        if let url = URL(string: "https://deltasquad.co/store/privacy.php?n=MakeYourFilter") {
            UIApplication.shared.open(url)
        }
    }
    
    func trySignUp() {
        //TODO SIGN UP FUNCTIONALITY
    }
}

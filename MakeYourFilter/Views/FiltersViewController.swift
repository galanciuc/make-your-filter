//
//  FiltersViewController.swift
//  MakeYourFilter
//
//  Created by Tolea on 1/15/20.
//  Copyright © 2020 Delta Squad. All rights reserved.
//

import UIKit
import SwiftCSV

class FiltersViewController: UIViewController,  UICollectionViewDelegateFlowLayout, UICollectionViewDelegate, UICollectionViewDataSource {
    
    var filterList : [FilterObject] = [];
    
    @IBOutlet var collectionView: UICollectionView!
    
    var selectedCell = -1;

    override func viewDidLoad() {
        super.viewDidLoad()
        
        guard let contentsOfUrl = Bundle.main.url(forResource: "filters", withExtension: "csv") else {
            return
        }
        
        do{
        
            let contents = try String(contentsOf: contentsOfUrl)
            
            let csv = try CSV(string: contents)
            
            self.filterList = []
            
            for row in csv.namedRows {
                
                let filter = FilterObject.init(name: row["filter_name"]!, type: Int(row["filter_type"]!)!, red: Float(row["filter_r"]!)!, green: Float(row["filter_g"]!)!, blue: Float(row["filter_b"]!)!, opacity: Float(row["filter_opacity"]!)!, intensity: 1.0)
                
                self.filterList.append(filter)
                
            }
        }
        catch{
            
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.collectionView.reloadData()
        
        self.startSettingFilterToImage()
    }
    
    let myfFilter = MYFFilter()
    let sharedContext = CIContext(options: [.useSoftwareRenderer : false])
    
    func resizedImage(image: CIImage, scale: CGFloat, aspectRatio: CGFloat) -> UIImage? {

        let filter = CIFilter(name: "CILanczosScaleTransform")
        filter?.setValue(image, forKey: kCIInputImageKey)
        filter?.setValue(scale, forKey: kCIInputScaleKey)
        filter?.setValue(aspectRatio, forKey: kCIInputAspectRatioKey)

        guard let outputCIImage = filter?.outputImage,
            let outputCGImage = sharedContext.createCGImage(outputCIImage,
                                                            from: outputCIImage.extent)
        else {
            return nil
        }

        return UIImage(cgImage: outputCGImage)
    }
    
    func startSettingFilterToImage() {
        
        var image: CIImage?

        if let parentVC = self.parent as? ExploreViewController {
            
            let uiImage = resizedImage(image: parentVC.ciImage!, scale: 0.1, aspectRatio: 1)
            
            image = CIImage(image: uiImage!)
        }
        else if let parentVC = self.parent as? EditImageViewController {
            let uiImage = resizedImage(image: parentVC.ciImage!, scale: 0.1, aspectRatio: 1)
            
            image = CIImage(image: uiImage!)
        }
        
        myfFilter.inputImage = image
        
        DispatchQueue.global(qos: .background).asyncAfter(deadline: .now() + 0.1) {

            for (index, filter) in self.filterList.enumerated() {

//                let value = filter.filter_value
//                let saturation = filter.filter_saturation
//                let hue = filter.filter_hue
//
//                var r : Float = 0;
//                var g : Float = 0;
//                var b : Float = 0;
//
//                if saturation == 0 {
//                    r = value;
//                    g = value;
//                    b = value;
//                }
//                else{
//
//                    let angle = (hue >= 360.0 ? 0 : hue);
//                    let sector = angle / 60.0 // Sector
//                    let i = floor(sector);
//                    let f = sector - i;
//
//                    let p = value * (1.0 - saturation);
//                    let q = value * (1.0 - (saturation * f));
//                    let t = value * (1.0 - (saturation * (1.0 - f)));
//
//                    switch(i) {
//                    case 0:
//                        r = value;
//                        g = t;
//                        b = p;
//                    case 1:
//                        r = q;
//                        g = value;
//                        b = p;
//                    case 2:
//                        r = p;
//                        g = value;
//                        b = t;
//                    case 3:
//                        r = p;
//                        g = q;
//                        b = value;
//                    case 4:
//                        r = t;
//                        g = p;
//                        b = value;
//                    default:
//                        r = value;
//                        g = p;
//                        b = q;
//                    }
//                }

                let filteredCIImage = self.myfFilter.outputMYF(filter_type: filter.filter_type, selectedR: filter.filter_r, selectedG: filter.filter_g, selectedB: filter.filter_b, selectedAlpha: filter.filter_opacity)

                let filteredUIImage = UIImage(ciImage: filteredCIImage!)

                filter.setImage(image: filteredUIImage)
                
                DispatchQueue.main.async {
                    var indexPaths: [IndexPath] = []
                    let indexPath = IndexPath.init(row: index, section: 0)
                    indexPaths.append(indexPath)
                    self.collectionView.reloadItems(at: indexPaths)
                }
            }
        }
    }
    
    func setupSelectedFilterValues(filter: FilterObject) {
        
        if let parentVC = parent as? ExploreViewController {
            parentVC.selectedR = filter.filter_r
            parentVC.selectedG = filter.filter_g
            parentVC.selectedB = filter.filter_b
            parentVC.selectedFilterType = filter.filter_type;
            parentVC.selectedIntensity = filter.filter_intensity;
            parentVC.selectedAlpha = filter.filter_opacity
        }
        else if let parentVC = parent as? EditImageViewController {
            parentVC.selectedR = filter.filter_r
            parentVC.selectedG = filter.filter_g
            parentVC.selectedB = filter.filter_b
            parentVC.selectedFilterType = filter.filter_type;
            parentVC.selectedIntensity = filter.filter_intensity;
            parentVC.selectedAlpha = filter.filter_opacity
        }
        
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return filterList.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "cell", for: indexPath) as? FilterCollectionViewCell
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.001, execute: {
            cell?.setImageCorner()
            self.view.layoutIfNeeded()
        })
        
        if filterList[indexPath.row].filter_image_loaded {
            cell?.activityIndicator.isHidden = true
            cell?.activityIndicator.stopAnimating()
            cell?.imageView.isHidden = false
            cell?.imageView.image = filterList[indexPath.row].filter_image
        }
        else{
            cell?.imageView.isHidden = true
            cell?.activityIndicator.isHidden = false
            cell?.activityIndicator.startAnimating()
        }
        
        cell?.setLabel(with: "MYF_" + filterList[indexPath.row].filter_name)
        
        if indexPath.row == selectedCell {
            cell?.editView.isHidden = false
        }
        else {
            cell?.editView.isHidden = true
        }

        return cell!
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        if (indexPath.row == selectedCell) {
            if let parentVC = parent as? ExploreViewController {
                parentVC.openMakeYourFilter()
            }
            else if let parentVC = parent as? EditImageViewController {
                parentVC.openMakeYourFilter()
            }
        }
        else{
            selectedCell = indexPath.row
            
            let indexPaths = collectionView.indexPathsForVisibleItems
            collectionView.reloadItems(at: indexPaths)
            
            self.setupSelectedFilterValues(filter: self.filterList[indexPath.row])
        }
        
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: 85, height: 85)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        return UIEdgeInsets(top: 0, left: 15, bottom: 0, right: 15)
    }

}

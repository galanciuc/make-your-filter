//
//  RootViewController.swift
//  MakeYourFilter
//
//  Created by Victoria on 6/29/19.
//  Copyright © 2019 Delta Squad. All rights reserved.
//

import UIKit

class RootViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        let viewController = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "MainVC") as! MainViewController
        self.navigationController?.pushViewController(viewController, animated: true)

    }
    



}

//
//  LightLeaksViewController.swift
//  MakeYourFilter
//
//  Created by Victoria on 6/30/19.
//  Copyright © 2019 Delta Squad. All rights reserved.
//

import UIKit

class LightLeaksViewController: UIViewController {

    
    @IBOutlet var buttonsStackView: UIStackView!
    @IBOutlet var radialButton: UIButton!
    @IBOutlet var linearButton: UIButton!
    
    
    @IBOutlet var label: UILabel!
    @IBOutlet var sliderStackView: UIStackView!
    
    @IBOutlet var sliders: [UISlider]!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        sliderStackView.isHidden = true
        label.isHidden = true
        for slider in sliders {
            self.setSlider(slider: slider)

        }

    }
    
    func setSlider(slider: UISlider) {
        slider.maximumValue = 100
        slider.minimumValue = 0
        slider.maximumTrackTintColor = .gray
        slider.minimumTrackTintColor = .white
    }
    
    func showSliderView() {
        sliderStackView.isHidden = false
        label.isHidden = false
        buttonsStackView.isHidden = true
    }
    
    @IBAction func radialButtonAction(_ sender: Any) {
        showSliderView()
    }
    
    
    
    
  
    @IBAction func linearButtonAction(_ sender: Any) {
        showSliderView()
    }
    
    
}

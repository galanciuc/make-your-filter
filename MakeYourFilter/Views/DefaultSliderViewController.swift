//
//  DefaultSliderViewController.swift
//  MakeYourFilter
//
//  Created by Victoria on 6/30/19.
//  Copyright © 2019 Delta Squad. All rights reserved.
//

import UIKit

class DefaultSliderViewController: UIViewController {
    
    @IBOutlet var brightnessSlider: UISlider!
    @IBOutlet var contrastSlider: UISlider!
    @IBOutlet var shadowsSlider: UISlider!
    @IBOutlet weak var highlightsSlider: UISlider!
    @IBOutlet var luxSlider: UISlider!
    
    @IBOutlet var brightnessLabel: UILabel!
    @IBOutlet var contrastLabel: UILabel!
    @IBOutlet var shadowsLabel: UILabel!
    @IBOutlet var highlightsLabel: UILabel!
    @IBOutlet var luxLabel: UILabel!
    
    var index = 0
    
    
    @IBAction func brightnessStartedChanging(_ sender: UISlider) {
        if let parentVC = parent as? ExploreViewController {
            parentVC.addHistoryUndoSpecificSettings()
        }
        else if let parentVC = parent as? EditImageViewController{
            parentVC.addHistoryUndoSpecificSettings()
        }
    }
    
    @IBAction func contrastStartedChanging(_ sender: UISlider) {
        if let parentVC = parent as? ExploreViewController {
            parentVC.addHistoryUndoSpecificSettings()
        }
        else if let parentVC = parent as? EditImageViewController{
            parentVC.addHistoryUndoSpecificSettings()
        }
    }
    
    @IBAction func shadowsStartedChanging(_ sender: UISlider) {
        if let parentVC = parent as? ExploreViewController {
            parentVC.addHistoryUndoSpecificSettings()
        }
        else if let parentVC = parent as? EditImageViewController{
            parentVC.addHistoryUndoSpecificSettings()
        }
    }
    
    @IBAction func highlightsStartedChanging(_ sender: UISlider) {
        if let parentVC = parent as? ExploreViewController {
            parentVC.addHistoryUndoSpecificSettings()
        }
        else if let parentVC = parent as? EditImageViewController{
            parentVC.addHistoryUndoSpecificSettings()
        }
    }
    
    @IBAction func luxStartedChanging(_ sender: UISlider) {
        if let parentVC = parent as? ExploreViewController {
            parentVC.addHistoryUndoSpecificSettings()
        }
        else if let parentVC = parent as? EditImageViewController{
            parentVC.addHistoryUndoSpecificSettings()
        }
    }
    
    @IBAction func brightnessChanged(_ sender: UISlider) {
        
        let shownText = getShownText(value: sender.value)
        
        self.brightnessLabel.text = shownText
        
        if let parentVC = parent as? ExploreViewController {
            parentVC.selectedBrightness = (sender.value - 100) / 100;
        }
        else if let parentVC = parent as? EditImageViewController {
            parentVC.selectedBrightness = (sender.value - 100) / 100;
        }
    }
    
    @IBAction func contrastChanged(_ sender: UISlider) {
        
        let shownText = getShownText(value: sender.value);
        
        self.contrastLabel.text = shownText;
        
        if let parentVC = parent as? ExploreViewController {
            parentVC.selectedContrast = ((sender.value - 100) / 100) + 1
        }
        else if let parentVC = parent as? EditImageViewController {
            parentVC.selectedContrast = ((sender.value - 100) / 100) + 1
        }
        
    }
    
    @IBAction func shadowsChanged(_ sender: UISlider) {
        
        let shownText = getShownText(value: sender.value)
        
        self.shadowsLabel.text = shownText
        
        if let parentVC = parent as? ExploreViewController {
            parentVC.selectedShadow = (sender.value - 100) / 100
        }
        else if let parentVC = parent as? EditImageViewController {
            parentVC.selectedShadow = (sender.value - 100) / 100
        }
        
    }
    
    @IBAction func highlightsChanged(_ sender: UISlider) {
        
        let shownText = getShownText(value: sender.value)
        
        self.highlightsLabel.text = shownText
        
        if let parentVC = parent as? ExploreViewController {
            parentVC.selectedHighlight = (sender.value - 100) / 100
        }
        else if let parentVC = parent as? EditImageViewController {
            parentVC.selectedHighlight = (sender.value - 100) / 100
        }
        
    }
    
    @IBAction func luxChanged(_ sender: UISlider) {
        
        let shownText = getShownText(value: sender.value)
        
        self.luxLabel.text = shownText
        
        if let parentVC = parent as? ExploreViewController {
            parentVC.selectedLux = (sender.value - 100) / 100
        }
        else if let parentVC = parent as? EditImageViewController {
            parentVC.selectedLux = (sender.value - 100) / 100
        }
        
    }
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.setLightSlider(slider: brightnessSlider)
        self.setLightSlider(slider: contrastSlider)
        self.setLightSlider(slider: shadowsSlider)
        self.setLightSlider(slider: highlightsSlider)
        self.setLightSlider(slider: luxSlider)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        
        self.setupSliders();
    }
    
    func setLightSlider(slider: UISlider) {
        slider.maximumValue = 200
        slider.minimumValue = 0
        slider.value = 100
        slider.maximumTrackTintColor = .gray
        slider.setThumbImage(#imageLiteral(resourceName: "thumb_image"), for: UIControl.State.normal)
        slider.minimumTrackTintColor = .white
    }
    
    func getShownText(value: Float) -> String {
        let shownValue = value - 100;
        
        var shownText = "";
        
        if(shownValue > 0) {
            shownText = shownText + "+"
        }
        
        let shownValue2Precisions = round(100 * shownValue) / 100
        
        shownText = shownText + String(shownValue2Precisions)
        
        return shownText
    }
    
    func setupLabels(brightness: Float, contrast: Float, shadows: Float, highlights: Float, lux: Float) {
        
        let shownTextBrightness = getShownText(value: brightness)
        
        self.brightnessLabel.text = shownTextBrightness
        
        let shownTextContrast = getShownText(value: contrast)
        
        self.contrastLabel.text = shownTextContrast
        
        let shownTextShadows = getShownText(value: shadows)
        
        self.shadowsLabel.text = shownTextShadows
        
        let shownTextHighlights = getShownText(value: highlights)
        
        self.highlightsLabel.text = shownTextHighlights
        
        let shownTextLux = getShownText(value: lux)
        
        self.luxLabel.text = shownTextLux
        
    }
    
    func setupUI(brightness: Float, contrast: Float, shadows: Float, highlights: Float, lux: Float) {
        
        self.setupLabels(brightness: brightness, contrast: contrast, shadows: shadows, highlights: highlights, lux: lux)
        
        brightnessSlider.value = brightness;
        contrastSlider.value = contrast;
        shadowsSlider.value = shadows;
        highlightsSlider.value = highlights;
        luxSlider.value = lux;
    }

    func setupSliders(){
        
        if let parentVC = parent as? ExploreViewController {
            let brightness = (parentVC.selectedBrightness * 100) + 100;
            let contrast = (parentVC.selectedContrast - 1) * 100 + 100;
            let shadows = (parentVC.selectedShadow * 100 + 100)
            let highlight = (parentVC.selectedHighlight * 100 + 100)
            let lux = (parentVC.selectedLux * 100 + 100)
            
            self.setupUI(brightness: brightness, contrast: contrast, shadows: shadows, highlights: highlight, lux: lux)
        }
        else if let parentVC = parent as? EditImageViewController {
            let brightness = (parentVC.selectedBrightness * 100) + 100;
            let contrast = (parentVC.selectedContrast - 1) * 100 + 100;
            let shadows = (parentVC.selectedShadow * 100 + 100)
            let highlight = (parentVC.selectedHighlight * 100 + 100)
            let lux = (parentVC.selectedLux * 100 + 100)
            
            self.setupUI(brightness: brightness, contrast: contrast, shadows: shadows, highlights: highlight, lux: lux)
        }
    }
}

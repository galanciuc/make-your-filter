//
// MakeYourFilterViewController MakeYourFilterViewController.swift
//  MakeYourFilter
//
//  Created by Victoria on 6/29/19.
//  Copyright © 2019 Delta Squad. All rights reserved.
//

import UIKit

class DefaultViewController: UIViewController, UICollectionViewDelegateFlowLayout, UICollectionViewDelegate, UICollectionViewDataSource {
    
    
    var index = 0
    var selectedCell = -1
    var presetsNames = ["Beach", "Sky", "Winter", "Rain", "Food"]
    
    @IBOutlet var collectionView: UICollectionView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return 10
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "cell", for: indexPath) as? MakeYourFilterCollectionViewCell
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.01, execute: {
            cell?.setImageCorner()
            self.view.layoutIfNeeded()
        })

        if index == 1 { // My filters

            if indexPath.row == 0 {
                cell?.imageView.image = UIImage(named: "icon")
                cell?.label.isHidden = true
            } else {
                cell?.imageView.image = UIImage(named: "")
                cell?.label.isHidden = false
                cell?.setLabel(with: "Name")
            }
            
            if indexPath.row == selectedCell {
                cell?.label.textColor = .white
            } else {
                cell?.label.textColor = .lightGray
            }
        } else if index == 7 { // Filters
            cell?.setLabel(with: "Filter \(indexPath.row + 1)")
        }

        return cell!
    }
    
    func collectionView(_ collectionView: UICollectionView, didDeselectItemAt indexPath: IndexPath) {
        
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        selectedCell = indexPath.row
        collectionView.reloadData()
        
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: 85, height: 85)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        return UIEdgeInsets(top: 0, left: 15, bottom: 0, right: 15)
    }
    
    
    
}

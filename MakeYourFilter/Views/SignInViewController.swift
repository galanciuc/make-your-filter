//
//  SignInViewController.swift
//  MakeYourFilter
//
//  Created by Tolea on 12/30/19.
//  Copyright © 2019 Delta Squad. All rights reserved.
//

import UIKit

class SignInViewController: UIViewController {
    
    @IBOutlet weak var emailField: UITextField!
    @IBOutlet weak var passwordField: UITextField!
    @IBOutlet weak var showButton: UIButton!
    
    var secureTextEntry = true;
    
    @IBAction func backAction(_ sender: Any) {
        self.navigationController?.popViewController(animated: true);
    }
    
    @IBAction func openSignUpAction(_ sender: Any) {
        self.openSignUp();
    }
    
    @IBAction func showPassword(_ sender: Any) {
        self.changePasswordVisibility();
    }
    
    @IBAction func signInAction(_ sender: Any) {
        self.trySignIn();
    }
    
    @IBAction func termsAndConditionsAction(_ sender: Any) {
        self.openTermsAndConditions();
    }
    
    @IBAction func privacyPolicyAction(_ sender: Any) {
        self.openPrivacyPolicy();
    }
    
    @IBAction func forgotPasswordAction(_ sender: Any) {
        self.openForgotPassword();
    }

    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    func openPrivacyPolicy() {
        if let url = URL(string: "https://deltasquad.co/store/privacy.php?n=MakeYourFilter") {
            UIApplication.shared.open(url)
        }
    }
    
    func openTermsAndConditions() {
        if let url = URL(string: "https://deltasquad.co/store/terms.php?n=MakeYourFilter") {
            UIApplication.shared.open(url)
        }
    }
    
    func openForgotPassword() {
        //TODO OPEN FORGOT PASSWORD
    }
    
    func trySignIn() {
        //TODO SIGN IN FUNCTIONALITY
    }
    
    func changePasswordVisibility() {
        secureTextEntry = !secureTextEntry
        passwordField.isSecureTextEntry = secureTextEntry
        showButton.setTitle(secureTextEntry ? "show" : "hide", for: UIControl.State.normal)
    }
    
    func openSignUp() {
        let viewController = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "SignUpVC") as! SignUpViewController
        self.navigationController?.pushViewController(viewController, animated: true);
    }

}

//
//  MakeYourPhotosGreatAgainViewController.swift
//  MakeYourFilter
//
//  Created by Tolea on 1/5/20.
//  Copyright © 2020 Delta Squad. All rights reserved.
//

import UIKit

class MakeYourPhotosGreatAgainViewController: UIViewController {
    
    @IBAction func backAction(_ sender: Any) {
        self.navigationController?.popViewController(animated: true);
    }
    
    @IBAction func openSignUpAction(_ sender: Any) {
        self.openSignUp();
    }

    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    func openSignUp() {
        let viewController = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "SignUpVC") as! SignUpViewController
        self.navigationController?.pushViewController(viewController, animated: true);
    }

}

//
//  MyFiltersViewController.swift
//  MakeYourFilter
//
//  Created by Tolea on 2/18/20.
//  Copyright © 2020 Delta Squad. All rights reserved.
//

import UIKit

class MyFiltersViewController: UIViewController, UICollectionViewDelegateFlowLayout, UICollectionViewDelegate, UICollectionViewDataSource {
    
    var filterList : [MyFilterObject] = []
    
    @IBOutlet var collectionView: UICollectionView!
    @IBOutlet var noFiltersYet: UILabel!
    
    var selectedCell = -1;

    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.retrieveFilters()
    }
    
    func retrieveFilters() {
        let userDefaults = UserDefaults.standard
        
        do{
        
            let decoded  = userDefaults.data(forKey: "my_filters")
            let decodedFilters = try NSKeyedUnarchiver.unarchiveTopLevelObjectWithData(decoded!) as! [MyFilterObject]

            filterList = decodedFilters
            
            if filterList.count == 0 {
                self.noFiltersYet.isHidden = false
                self.collectionView.isHidden = true
            }
            else {
                self.noFiltersYet.isHidden = true
                self.collectionView.isHidden = false
                self.collectionView.reloadData()
            }
            
        }
        catch {
            self.noFiltersYet.isHidden = false
            self.collectionView.isHidden = true
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.startSettingFilterToImage()
    }
    
    let myfFilter = MYFFilter()
    let sharedContext = CIContext(options: [.useSoftwareRenderer : false])
    
    func resizedImage(image: CIImage, scale: CGFloat, aspectRatio: CGFloat) -> UIImage? {

        let filter = CIFilter(name: "CILanczosScaleTransform")
        filter?.setValue(image, forKey: kCIInputImageKey)
        filter?.setValue(scale, forKey: kCIInputScaleKey)
        filter?.setValue(aspectRatio, forKey: kCIInputAspectRatioKey)

        guard let outputCIImage = filter?.outputImage,
            let outputCGImage = sharedContext.createCGImage(outputCIImage,
                                                            from: outputCIImage.extent)
        else {
            return nil
        }

        return UIImage(cgImage: outputCGImage)
    }
    
    func startSettingFilterToImage() {
        
        var image: CIImage?

        if let parentVC = self.parent as? EditImageViewController {
            let uiImage = resizedImage(image: parentVC.ciImage!, scale: 0.1, aspectRatio: 1)
            
            image = CIImage(image: uiImage!)
        }
        
        myfFilter.inputImage = image
        
        DispatchQueue.global(qos: .background).asyncAfter(deadline: .now() + 2) {

            for (index, filter) in self.filterList.enumerated() {

                let filteredCIImage = self.myfFilter.outputMYF(filter_type: filter.filter_type, selectedR: filter.filter_r, selectedG: filter.filter_g, selectedB: filter.filter_b, selectedAlpha: filter.filter_opacity)

                let filteredUIImage = UIImage(ciImage: filteredCIImage!)

                filter.setImage(image: filteredUIImage)
                
                DispatchQueue.main.async {
                    var indexPaths: [IndexPath] = []
                    let indexPath = IndexPath.init(row: index, section: 0)
                    indexPaths.append(indexPath)
                    self.collectionView.reloadItems(at: indexPaths)
                }
            }
        }
    }
    
    func setupSelectedFilterValues(filter: MyFilterObject) {
        guard let parentVC = parent as? EditImageViewController else {
                       
            return;
        }
        
        parentVC.selectedFilterType = filter.filter_type
        parentVC.selectedR = filter.filter_r
        parentVC.selectedG = filter.filter_g
        parentVC.selectedB = filter.filter_b
        parentVC.selectedAlpha = filter.filter_opacity
        
    }

    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return filterList.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "cell", for: indexPath) as? FilterCollectionViewCell
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.01, execute: {
            cell?.setImageCorner()
            self.view.layoutIfNeeded()
        })
        
        if filterList[indexPath.row].filter_image_loaded {
            cell?.activityIndicator.isHidden = true
            cell?.activityIndicator.stopAnimating()
            cell?.imageView.isHidden = false
            cell?.imageView.image = filterList[indexPath.row].filter_image
        }
        else{
            cell?.imageView.isHidden = true
            cell?.activityIndicator.isHidden = false
            cell?.activityIndicator.startAnimating()
        }
        
        cell?.setLabel(with: filterList[indexPath.row].filter_name)
        
        if indexPath.row == selectedCell {
            cell?.editView.isHidden = false
        }
        else {
            cell?.editView.isHidden = true
        }

        return cell!
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        if (indexPath.row == selectedCell) {
            if let parentVC = parent as? EditImageViewController {
                parentVC.openMakeYourFilter()
            }
        }
        else{
            selectedCell = indexPath.row
            
            let indexPaths = collectionView.indexPathsForVisibleItems
            collectionView.reloadItems(at: indexPaths)
            
            self.setupSelectedFilterValues(filter: self.filterList[indexPath.row])
        }
        
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: 85, height: 85)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        return UIEdgeInsets(top: 0, left: 15, bottom: 0, right: 15)
    }
}

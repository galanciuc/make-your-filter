//
//  HelperFunctions.swift
//  MakeYourFilter
//
//  Created by Victoria on 6/22/19.
//  Copyright © 2019 Delta Squad. All rights reserved.
//

import UIKit

var purple = #colorLiteral(red: 0.5882352941, green: 0.2156862745, blue: 0.5215686275, alpha: 1)
var screenWidth = UIScreen.main.bounds.width
var screenHeight = UIScreen.main.bounds.height
var UD = UserDefaults.standard
var importedPhotos = UD.value(forKey: "importedPhotos")

extension UIViewController {
    func addChildController(content: UIViewController, frame: CGRect) {
        addChild(content)
        content.view.frame = frame
        self.view.addSubview(content.view)
        content.didMove(toParent: self)
    }
    
    func removeChildController(content: UIViewController) {
        content.willMove(toParent: nil)
        content.view.removeFromSuperview()
        content.removeFromParent()
    }
    
    
}


open class CustomSlider : UISlider {
    @IBInspectable open var trackWidth:CGFloat = 6 {
        didSet {setNeedsDisplay()}
    }
    
    override open func trackRect(forBounds bounds: CGRect) -> CGRect {
        let defaultBounds = super.trackRect(forBounds: bounds)
        return CGRect(
            x: defaultBounds.origin.x,
            y: defaultBounds.origin.y + defaultBounds.size.height/2 - trackWidth/2,
            width: defaultBounds.size.width,
            height: trackWidth
        )
    }
}

//
//  GraphicContext.swift
//  MakeYourFilter
//
//  Created by Tolea on 1/5/20.
//  Copyright © 2020 Delta Squad. All rights reserved.
//

import Foundation

import Metal
import MetalKit

class GraphicContext {
    
    var mLibrary: MTLLibrary?
    var mDevice: MTLDevice?
    var mCommandQueue: MTLCommandQueue?
    var mTextureLoader: MTKTextureLoader?
    
    init() {
        mDevice = MTLCreateSystemDefaultDevice();
        mLibrary = mDevice?.makeDefaultLibrary();
        mCommandQueue = mDevice?.makeCommandQueue();
        mTextureLoader = MTKTextureLoader(device: mDevice!);
    }
    
}

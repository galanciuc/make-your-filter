//
//  EditCIKernel.metal
//  MakeYourFilter
//
//  Created by Tolea on 1/24/20.
//  Copyright © 2020 Delta Squad. All rights reserved.
//

#include <metal_stdlib>
#include <CoreImage/CoreImage.h>
using namespace metal;

extern "C" { namespace coreimage {
    
    //MARK: FILTERS
    float4 filterShader(sample_t input, float filter_type, float mixcolorR, float mixcolorG, float mixcolorB, float mixcolorA) {
        
        float3 srgbValues = linear_to_srgb(input.rgb);
        
        float4 mixcolor = float4(mixcolorR, mixcolorG, mixcolorB, mixcolorA);
        
        float newR6 = srgbValues.r;
        float newG6 = srgbValues.g;
        float newB6 = srgbValues.b;
        
        float newR7;
        float newG7;
        float newB7;
        
        //MARK: NORMAL FILTER
        if(filter_type == 0.0f){
            newR7 = mix(newR6, mixcolor.r, mixcolor.a);
            newG7 = mix(newG6, mixcolor.g, mixcolor.a);
            newB7 = mix(newB6, mixcolor.b, mixcolor.a);
        }
        //MARK: OVERLAY FILTER
        else if(filter_type == 1){
            newR7 = newR6 < 0.5 ? (2.0 * newR6 * mixcolor.r * mixcolor.a) : (1.0 - 2.0 * (1.0 - newR6) * (1.0 - mixcolor.r * mixcolor.a));
            newG7 = newG6 < 0.5 ? (2.0 * newG6 * mixcolor.g * mixcolor.a) : (1.0 - 2.0 * (1.0 - newG6) * (1.0 - mixcolor.g * mixcolor.a));
            newB7 = newB6 < 0.5 ? (2.0 * newB6 * mixcolor.b * mixcolor.a) : (1.0 - 2.0 * (1.0 - newB6) * (1.0 - mixcolor.b * mixcolor.a));
        }
        //MARK: DIFFERENCE FILTER
        else if(filter_type == 2){
            newR7 = abs(newR6 - mixcolor.r * mixcolor.a);
            newG7 = abs(newG6 - mixcolor.g * mixcolor.a);
            newB7 = abs(newB6 - mixcolor.b * mixcolor.a);
        }
        //MARK: SCREEN FILTER
        else if(filter_type == 3){
            newR7 = 1.0 - (1.0 - newR6) * (1.0 - mixcolor.r * mixcolor.a);
            newG7 = 1.0 - (1.0 - newG6) * (1.0 - mixcolor.g * mixcolor.a);
            newB7 = 1.0 - (1.0 - newB6) * (1.0 - mixcolor.b * mixcolor.a);
        }
        //MARK: MULTIPLY FILTER
        else if(filter_type == 4){
            newR7 = newR6 * mixcolor.r * mixcolor.a;
            newG7 = newG6 * mixcolor.g * mixcolor.a;
            newB7 = newB6 * mixcolor.b * mixcolor.a;
        }
        //MARK: DODGE FILTER
        else if(filter_type == 5){
            newR7 = newR6 / (1.0 - mixcolor.r * mixcolor.a);
            newG7 = newG6 / (1.0 - mixcolor.g * mixcolor.a);
            newB7 = newB6 / (1.0 - mixcolor.b * mixcolor.a);
        }
        //MARK: EXCLUSION FILTER
        else if(filter_type == 6){
            newR7 = newR6 + mixcolor.r * mixcolor.a - 2.0 * newR6 * mixcolor.r * mixcolor.a;
            newG7 = newG6 + mixcolor.g * mixcolor.a - 2.0 * newG6 * mixcolor.g * mixcolor.a;
            newB7 = newB6 + mixcolor.b * mixcolor.a - 2.0 * newB6 * mixcolor.b * mixcolor.a;
        }
        //MARK: DARKEN FILTER
        else if(filter_type == 7){
            newR7 = newR6 <= mixcolor.r * mixcolor.a ? newR6 : mixcolor.r * mixcolor.a;
            newG7 = newG6 <= mixcolor.g * mixcolor.a ? newG6 : mixcolor.g * mixcolor.a;
            newB7 = newB6 <= mixcolor.b * mixcolor.a ? newB6 : mixcolor.b * mixcolor.a;
        }
        //MARK: SOFT LIGHT FILTER
        else if(filter_type == 8){
            newR7 = mixcolor.r * mixcolor.a < 0.5 ? (2.0 * newR6 * mixcolor.r * mixcolor.a + newR6 * newR6 * (1.0 - 2.0 * mixcolor.r * mixcolor.a)) : (sqrt(newR6) * (2.0 * mixcolor.r * mixcolor.a - 1.0) + (2.0 * newR6) * (1.0 - mixcolor.r * mixcolor.a));
            newG7 = mixcolor.g * mixcolor.a < 0.5 ? (2.0 * newG6 * mixcolor.g * mixcolor.a + newG6 * newG6 * (1.0 - 2.0 * mixcolor.g * mixcolor.a)) : (sqrt(newG6) * (2.0 * mixcolor.g * mixcolor.a - 1.0) + (2.0 * newG6) * (1.0 - mixcolor.g * mixcolor.a));
            newB7 = mixcolor.b * mixcolor.a < 0.5 ? (2.0 * newB6 * mixcolor.b * mixcolor.a + newB6 * newB6 * (1.0 - 2.0 * mixcolor.b * mixcolor.a)) : (sqrt(newB6) * (2.0 * mixcolor.b * mixcolor.a - 1.0) + (2.0 * newB6) * (1.0 - mixcolor.b * mixcolor.a));
        }
        //MARK: BURN FILTER
        else if(filter_type == 9){
            newR7 = 1.0 - (1.0 - newR6) / (mixcolor.r * mixcolor.a);
            newG7 = 1.0 - (1.0 - newG6) / (mixcolor.g * mixcolor.a);
            newB7 = 1.0 - (1.0 - newB6) / (mixcolor.b * mixcolor.a);
        }
        //MARK: LIGHTEN FILTER
        else if(filter_type == 10){
            newR7 = newR6 >= mixcolor.r * mixcolor.a ? newR6 : mixcolor.r * mixcolor.a;
            newG7 = newG6 >= mixcolor.g * mixcolor.a ? newG6 : mixcolor.g * mixcolor.a;
            newB7 = newB6 >= mixcolor.b * mixcolor.a ? newB6 : mixcolor.b * mixcolor.a;
        }
        //MARK: HARD LIGHT FILTER
        else if(filter_type == 11){
            newR7 = mixcolor.r * mixcolor.a < 0.5 ? (2.0 * newR6 * mixcolor.r * mixcolor.a) : (1.0 - 2.0 * (1.0 - newR6) * (1.0 - mixcolor.r * mixcolor.a));
            newG7 = mixcolor.g * mixcolor.a < 0.5 ? (2.0 * newG6 * mixcolor.g * mixcolor.a) : (1.0 - 2.0 * (1.0 - newG6) * (1.0 - mixcolor.g * mixcolor.a));
            newB7 = mixcolor.b * mixcolor.a < 0.5 ? (2.0 * newB6 * mixcolor.b * mixcolor.a) : (1.0 - 2.0 * (1.0 - newB6) * (1.0 - mixcolor.b * mixcolor.a));
        }
        //MARK: LINEAR BURN FILTER
        else if(filter_type == 12){
            newR7 = newR6 + mixcolor.r * mixcolor.a - 1.0;
            newG7 = newG6 + mixcolor.g * mixcolor.a - 1.0;
            newB7 = newB6 + mixcolor.b * mixcolor.a - 1.0;
        }
        //MARK: ADDITION FILTER
        else if(filter_type == 13){
            newR7 = newR6 + mixcolor.r * mixcolor.a <= 1.0 ? newR6 + mixcolor.r * mixcolor.a : 1.0;
            newG7 = newG6 + mixcolor.g * mixcolor.a <= 1.0 ? newG6 + mixcolor.g * mixcolor.a : 1.0;
            newB7 = newB6 + mixcolor.b * mixcolor.a <= 1.0 ? newB6 + mixcolor.b * mixcolor.a : 1.0;
        }
        //MARK: DIVIDE FILTER
        else if(filter_type == 14){
            newR7 = newR6 / (mixcolor.r * mixcolor.a);
            newG7 = newG6 / (mixcolor.g * mixcolor.a);
            newB7 = newB6 / (mixcolor.b * mixcolor.a);
        }
        //MARK: SUBSTRACT FILTER
        else if(filter_type == 15){
            newR7 = newR6 - mixcolor.r * mixcolor.a >= 0.0 ? newR6 - mixcolor.r * mixcolor.a : 0.0;
            newG7 = newG6 - mixcolor.g * mixcolor.a >= 0.0 ? newG6 - mixcolor.g * mixcolor.a : 0.0;
            newB7 = newB6 - mixcolor.b * mixcolor.a >= 0.0 ? newB6 - mixcolor.b * mixcolor.a : 0.0;
        }
        //MARK: VIVID LIGHT FILTER
        else{
            newR7 = mixcolor.r * mixcolor.a <= 0.5 ? newR6 / (1.0 - mixcolor.r * mixcolor.a) : 1.0 - (1.0 - newR6) / (mixcolor.r * mixcolor.a);
            newG7 = mixcolor.g * mixcolor.a <= 0.5 ? newG6 / (1.0 - mixcolor.g * mixcolor.a) : 1.0 - (1.0 - newG6) / (mixcolor.g * mixcolor.a);
            newB7 = mixcolor.b * mixcolor.a <= 0.5 ? newB6 / (1.0 - mixcolor.b * mixcolor.a) : 1.0 - (1.0 - newB6) / (mixcolor.b * mixcolor.a);
        }
        
        float3 linearValues = srgb_to_linear(float3(newR7, newG7, newB7));
        
        float3 clampedResult = clamp(linearValues, float3(0,0,0), float3(1,1,1));
        
        return float4(clampedResult.r, clampedResult.g, clampedResult.b, 1.0);
    }
    
    //MARK: CONTRAST
    float4 contrastShader(sample_t input, float contrastValue) {
        
        float3 srgbValues = linear_to_srgb(input.rgb);
        
        float newR = (contrastValue * (srgbValues.r - 0.5) + 0.5);
        float newG = (contrastValue * (srgbValues.g - 0.5) + 0.5);
        float newB = (contrastValue * (srgbValues.b - 0.5) + 0.5);
        
        float3 linearValues = srgb_to_linear(float3(newR, newG, newB));
        
        float3 clampedValues = clamp(linearValues, float3(0,0,0), float3(1,1,1));
        
        return float4(clampedValues.r, clampedValues.g, clampedValues.b, 1.0);
    }
    
    //MARK: BRIGHTNESS
    float4 brightnessShader(sample_t input, float brightness) {
        
        float3 srgbValues = linear_to_srgb(input.rgb);
        
        float sum = srgbValues.r + srgbValues.g + srgbValues.b;
        float percent = sum / 3.0;
       
        float changeLuminosity = 0.2 * brightness;
       
        float newR = srgbValues.r + percent * brightness + changeLuminosity;
        float newG = srgbValues.g + percent * brightness + changeLuminosity;
        float newB = srgbValues.b + percent * brightness + changeLuminosity;
        
        float3 linearValues = srgb_to_linear(float3(newR, newG, newB));
        
        float3 clampedValues = clamp(linearValues, float3(0,0,0), float3(1,1,1));
        
        return float4(clampedValues.r, clampedValues.g, clampedValues.b, 1.0);
    }
    
    //MARK: TEMPERATURE & TINT
    float4 temperatureTintShader(sample_t input, float temperature, float tint) {
        
        float3 srgbValues = linear_to_srgb(input.rgb);
        
        float newR = srgbValues.r + temperature;
        float newG = srgbValues.g + tint;
        float newB = srgbValues.b - temperature;
        
        float3 linearValues = srgb_to_linear(float3(newR, newG, newB));
        
        float3 clampedValues = clamp(linearValues, float3(0,0,0), float3(1,1,1));
        
        return float4(clampedValues.r, clampedValues.g, clampedValues.b, 1.0);
    }
    
    //MARK: GAMMA
    float4 gammaShader(sample_t input, float gamma) {
        
        float3 srgbValues = linear_to_srgb(input.rgb);
        
        float3 newColorGamma = pow(srgbValues, float3(gamma));
        
        float3 linearValues = srgb_to_linear(newColorGamma);
        
        float3 clampedValues = clamp(linearValues, float3(0,0,0), float3(1,1,1));
        
        return float4(clampedValues.rgb, 1.0);
    }
    
    //MARK: VIBRANCE
    float4 vibranceShader(sample_t input, float vibrance) {
        
        float3 srgbValues = linear_to_srgb(input.rgb);
        
        float average = (srgbValues.r + srgbValues.g + srgbValues.b) / 3.0;
        float mx = max(srgbValues.r, max(srgbValues.g, srgbValues.b));
        float amt = (mx - average) * (vibrance * 3.0);
        
        float3 newColorVibrance = mix(srgbValues.rgb, float3(mx), amt);
        
        float3 linearValues = srgb_to_linear(newColorVibrance);
        
        float3 clampedValues = clamp(linearValues, float3(0,0,0), float3(1,1,1));
        
        return float4(clampedValues.rgb, 1.0);
    }
    
    //MARK: SHADOWS & HIGHLIGHTS
    float4 shadowsHighlightsShader(sample_t input, float shadows, float highlights) {
        
        float3 srgbValues = linear_to_srgb(input.rgb);
        
        float lumR = 0.299;
        float lumG = 0.587;
        float lumB = 0.114;
        
        float luminance2 = sqrt(lumR * pow(srgbValues.r, 2.0) + lumG * pow(srgbValues.g, 2.0) + lumB * pow(srgbValues.b, 2.0));
        float s = shadows * 0.05 * (pow(8.0, 1.0 - luminance2) - 1.0);
        float h = highlights * 0.05 * (pow(8.0, luminance2) - 1.0);
        
        float newR = srgbValues.r + s + h;
        float newG = srgbValues.g + s + h;
        float newB = srgbValues.b + s + h;
        
        float3 linearValues = srgb_to_linear(float3(newR, newG, newB));
        
        float3 clampedValues = clamp(linearValues, float3(0,0,0), float3(1,1,1));
        
        return float4(clampedValues.rgb, 1.0);
    }
    
    //MARK: SATURATION
    float4 saturationShader(sample_t input, float saturation) {
        
        float3 srgbValues = linear_to_srgb(input.rgb);
        
        const float3 W = float3(0.2125, 0.7154, 0.0721);
        float3 irgb = float3(srgbValues.r, srgbValues.g, srgbValues.b);
        float luminance = dot(irgb, W);
        float3 target_vec = float3(luminance, luminance, luminance);
        float4 newColors = float4(mix(target_vec, irgb, saturation), 1.0);
        float newR = newColors.r;
        float newG = newColors.g;
        float newB = newColors.b;
        
        float3 linearValues = srgb_to_linear(float3(newR, newG, newB));
        
        float3 clampedValues = clamp(linearValues, float3(0,0,0), float3(1,1,1));
        
        return float4(clampedValues.rgb, input.a);
    }
    
    //MARK: LUX
    float4 luxShader(sample_t input, float lux) {
        
        float3 srgbValues = linear_to_srgb(input.rgb);
        
        float newR = srgbValues.r + lux;
        float newG = srgbValues.g + lux;
        float newB = srgbValues.b + lux;
        
        float3 linearValues = srgb_to_linear(float3(newR, newG, newB));
        
        float3 clampedValues = clamp(linearValues, float3(0,0,0), float3(1,1,1));
        
        return float4(clampedValues.rgb, input.a);
    }
    
    //MARK: HUE
    float4 hueShader(sample_t input, float hue) {
        
        float3 srgbValues = linear_to_srgb(input.rgb);
        
        float4 beforeHueColor = float4(srgbValues.rgb, 1.0);
        
        float4 kRGBToYPrime = float4(0.299, 0.587, 0.114, 0.0);
        float4 kRGBToI = float4(0.595716, -0.274453, -0.321263, 0.0);
        float4 kRGBToQ = float4(0.211456, -0.522591, 0.31135, 0.0);

        float4 kYIQToR = float4(1.0, 0.9563, 0.6210, 0.0);
        float4 kYIQToG = float4(1.0, -0.2721, -0.6474, 0.0);
        float4 kYIQToB = float4(1.0, -1.1070, 1.7046, 0.0);
        
        float YPrime = dot(beforeHueColor, kRGBToYPrime);
        float I = dot(beforeHueColor, kRGBToI);
        float Q = dot(beforeHueColor, kRGBToQ);
        
        float color_hue = atan2(Q, I);
        float chroma = sqrt(I * I + Q * Q);
        
        color_hue += (-hue);
        
        Q = chroma * sin (color_hue);
        I = chroma * cos (color_hue);
        
        float4 yIQ = float4(YPrime, I, Q, 0.0);
        
        float4 newColorHue(dot(yIQ, kYIQToR), dot(yIQ, kYIQToG), dot(yIQ, kYIQToB), 1.0);
        
        float3 linearValues = srgb_to_linear(newColorHue.rgb);
        
        float3 clampedValues = clamp(linearValues, float3(0,0,0), float3(1,1,1));
        
        return float4(clampedValues.rgb, input.a);
    }
    
    //MARK: POSTERIZE
    float4 posterizeShader(sample_t input, float posterize) {
        float4 posterizeColors;
        
        float3 srgbValues = linear_to_srgb(input.rgb);
        
        if(posterize == 25.0) {
            posterizeColors = float4(srgbValues.r, srgbValues.g, srgbValues.b, input.a);
        }
        else{
            posterizeColors = floor((float4(srgbValues, input.a) * posterize) + float4(0.5)) / posterize;
        }
        
        float3 linearValues = srgb_to_linear(posterizeColors.rgb);
        
        float3 clampedValues = clamp(linearValues, float3(0,0,0), float3(1,1,1));
        
//        float normalizedR = posterizeColors.r >= 1.0 ? input.r : posterizeColors.r;
//        float normalizedG = posterizeColors.g >= 1.0 ? input.g : posterizeColors.g;
//        float normalizedB = posterizeColors.b >= 1.0 ? input.b : posterizeColors.b;
        
        return float4(clampedValues.r, clampedValues.g, clampedValues.b, 1.0);
    }
    
    //MARK: BLUR POWER
    float4 blurShader(sampler input, float blurPower) {
        float gaussFilterLowPassCoefficient = 0.015625 * (1.0 - (2.0 * blurPower));
        float gaussFilterMediumPassCoefficient = 0.09375 * (1.0 - (2.0 * blurPower));
        float gaussFilterHighPassCoefficient = 0.234375 * (1.0 - (2.0 * blurPower));
        float gaussFilterCurrentPassCoefficient = 1.6875 - 1.375 * (1.0 - blurPower);
        
        float size = input.size().x;

        const float2 gaussFilter[7] =
        {
            float2(-3.0 / size,    gaussFilterLowPassCoefficient),
            float2(-2.0 / size,    gaussFilterMediumPassCoefficient),
            float2(-1.0 / size,    gaussFilterHighPassCoefficient),
            float2(0.0,    gaussFilterCurrentPassCoefficient),
            float2(1.0 / size,    gaussFilterHighPassCoefficient),
            float2(2.0 / size,    gaussFilterMediumPassCoefficient),
            float2(3.0 / size,    gaussFilterLowPassCoefficient)
        };

        float4 colorBlur = float4(0.0);

        float2 u_Scale = float2(1.0, 2.0);

        for( int i = 0; i < 7; i++)
        {
            colorBlur += input.sample(float2( input.coord().x+gaussFilter[i].x*u_Scale.x, input.coord().y+gaussFilter[i].x*u_Scale.y ))*gaussFilter[i].y;
        }
        
        float normalizedRed = colorBlur.r > 1.0 ? abs(1.0 - colorBlur.r) : colorBlur.r;
        float normalizedGreen = colorBlur.g > 1.0 ? abs(1.0 - colorBlur.g) : colorBlur.g;
        float normalizedBlue = colorBlur.b > 1.0 ? abs(1.0 - colorBlur.b) : colorBlur.b;
        
        float3 clampedValues = clamp(colorBlur.rgb, float3(0,0,0), float3(1,1,1));
        
        return float4(clampedValues.r, clampedValues.g, clampedValues.b, 1.0);
    }
    
    float4 editShader(sample_t input, float filter_type, float mixcolorR, float mixcolorG, float mixcolorB, float mixcolorA, float contrast, float brightness, float shadows, float highlights, float lux, float temperature, float tint, float saturation, float vibrance, float gamma, float hue, float posterize, float filter_intensity) {
        
        float3 srgbValues = linear_to_srgb(input.rgb);
        
        float4 posterizeColors;
        
        //MARK: POSTERIZE
        
        if(posterize == 25.0) {
            posterizeColors = float4(srgbValues.r, srgbValues.g, srgbValues.b, input.a);
        }
        else{
            posterizeColors = floor((float4(srgbValues, input.a) * posterize) + float4(0.5)) / posterize;
        }

        //MARK: VIBRANCE

        float average = (posterizeColors.r + posterizeColors.g + posterizeColors.b) / 3.0;
        float mx = max(srgbValues.r, max(posterizeColors.g, posterizeColors.b));
        float amt = (mx - average) * (vibrance * 3.0);

        float3 newColorVibrance = mix(posterizeColors.rgb, float3(mx), amt);

        //MARK: GAMMA ADJUSTMENT

        float3 newColorGamma = pow(newColorVibrance, float3(gamma));

        //MARK: HUE ADJUSTMENT

        float4 beforeHueColor = float4(newColorGamma.rgb, 1.0);

        float4 kRGBToYPrime = float4(0.299, 0.587, 0.114, 0.0);
        float4 kRGBToI = float4(0.595716, -0.274453, -0.321263, 0.0);
        float4 kRGBToQ = float4(0.211456, -0.522591, 0.31135, 0.0);

        float4 kYIQToR = float4(1.0, 0.9563, 0.6210, 0.0);
        float4 kYIQToG = float4(1.0, -0.2721, -0.6474, 0.0);
        float4 kYIQToB = float4(1.0, -1.1070, 1.7046, 0.0);

        float YPrime = dot(beforeHueColor, kRGBToYPrime);
        float I = dot(beforeHueColor, kRGBToI);
        float Q = dot(beforeHueColor, kRGBToQ);

        float color_hue = atan2(Q, I);
        float chroma = sqrt(I * I + Q * Q);

        color_hue += (-hue);

        Q = chroma * sin (color_hue);
        I = chroma * cos (color_hue);

        float4 yIQ = float4(YPrime, I, Q, 0.0);

        float4 newColorHue(dot(yIQ, kYIQToR), dot(yIQ, kYIQToG), dot(yIQ, kYIQToB), 1.0);

        //MARK: TEMPERATURE & TINT

        float newR = newColorHue.r + temperature;
        float newG = newColorHue.g + tint;
        float newB = newColorHue.b - temperature;

        //MARK: SATURATION

        const float3 W = float3(0.2125, 0.7154, 0.0721);
        float3 irgb = float3(newR, newG, newB);
        float luminance = dot(irgb, W);
        float3 target_vec = float3(luminance, luminance, luminance);
        float4 newColors = float4(mix(target_vec, irgb, saturation), 1.0);
        float newR2 = newColors.r;
        float newG2 = newColors.g;
        float newB2 = newColors.b;

        //MARK: CONTRAST

        float newR3 = (contrast * (newR2 - 0.5) + 0.5);
        float newG3 = (contrast * (newG2 - 0.5) + 0.5);
        float newB3 = (contrast * (newB2 - 0.5) + 0.5);

        //MARK: BRIGHTNESS

        float sum = newR3 + newG3 + newB3;
        float percent = sum / 3.0;

        float changeLuminosity = 0.2 * brightness;

        float newR4 = newR3 + percent * brightness + changeLuminosity;
        float newG4 = newG3 + percent * brightness + changeLuminosity;
        float newB4 = newB3 + percent * brightness + changeLuminosity;

        //MARK: SHADOWS & HIGHLIGHTS

        float lumR = 0.299;
        float lumG = 0.587;
        float lumB = 0.114;

        float luminance2 = sqrt(lumR * pow(newR2, 2.0) + lumG * pow(newG2, 2.0) + lumB * pow(newB2, 2.0));
        float s = shadows * 0.05 * (pow(8.0, 1.0 - luminance2) - 1.0);
        float h = highlights * 0.05 * (pow(8.0, luminance2) - 1.0);

        float newR5 = newR4 + s + h;
        float newG5 = newG4 + s + h;
        float newB5 = newB4 + s + h;

        //MARK: LUX

        float newR6 = newR5 + lux;
        float newG6 = newG5 + lux;
        float newB6 = newB5 + lux;

        //MARK: FILTERS

        float4 mixcolor = float4(mixcolorR, mixcolorG, mixcolorB, mixcolorA);

        float newR7;
        float newG7;
        float newB7;

        //MARK: NORMAL FILTER
        if(filter_type == 0){
            newR7 = mix(newR6, mixcolor.r, mixcolor.a);
            newG7 = mix(newG6, mixcolor.g, mixcolor.a);
            newB7 = mix(newB6, mixcolor.b, mixcolor.a);
        }
        //MARK: OVERLAY FILTER
        else if(filter_type == 1){
            newR7 = newR6 < 0.5 ? (2.0 * newR6 * mixcolor.r * mixcolor.a) : (1.0 - 2.0 * (1.0 - newR6) * (1.0 - mixcolor.r * mixcolor.a));
            newG7 = newG6 < 0.5 ? (2.0 * newG6 * mixcolor.g * mixcolor.a) : (1.0 - 2.0 * (1.0 - newG6) * (1.0 - mixcolor.g * mixcolor.a));
            newB7 = newB6 < 0.5 ? (2.0 * newB6 * mixcolor.b * mixcolor.a) : (1.0 - 2.0 * (1.0 - newB6) * (1.0 - mixcolor.b * mixcolor.a));
        }
        //MARK: DIFFERENCE FILTER
        else if(filter_type == 2){
            newR7 = abs(newR6 - mixcolor.r * mixcolor.a);
            newG7 = abs(newG6 - mixcolor.g * mixcolor.a);
            newB7 = abs(newB6 - mixcolor.b * mixcolor.a);
        }
        //MARK: SCREEN FILTER
        else if(filter_type == 3){
            newR7 = 1.0 - (1.0 - newR6) * (1.0 - mixcolor.r * mixcolor.a);
            newG7 = 1.0 - (1.0 - newG6) * (1.0 - mixcolor.g * mixcolor.a);
            newB7 = 1.0 - (1.0 - newB6) * (1.0 - mixcolor.b * mixcolor.a);
        }
        //MARK: MULTIPLY FILTER
        else if(filter_type == 4){
            newR7 = newR6 * mixcolor.r * mixcolor.a;
            newG7 = newG6 * mixcolor.g * mixcolor.a;
            newB7 = newB6 * mixcolor.b * mixcolor.a;
        }
        //MARK: DODGE FILTER
        else if(filter_type == 5){
            newR7 = newR6 / (1.0 - mixcolor.r * mixcolor.a);
            newG7 = newG6 / (1.0 - mixcolor.g * mixcolor.a);
            newB7 = newB6 / (1.0 - mixcolor.b * mixcolor.a);
        }
        //MARK: EXCLUSION FILTER
        else if(filter_type == 6){
            newR7 = newR6 + mixcolor.r * mixcolor.a - 2.0 * newR6 * mixcolor.r * mixcolor.a;
            newG7 = newG6 + mixcolor.g * mixcolor.a - 2.0 * newG6 * mixcolor.g * mixcolor.a;
            newB7 = newB6 + mixcolor.b * mixcolor.a - 2.0 * newB6 * mixcolor.b * mixcolor.a;
        }
        //MARK: DARKEN FILTER
        else if(filter_type == 7){
            newR7 = newR6 <= mixcolor.r * mixcolor.a ? newR6 : mixcolor.r * mixcolor.a;
            newG7 = newG6 <= mixcolor.g * mixcolor.a ? newG6 : mixcolor.g * mixcolor.a;
            newB7 = newB6 <= mixcolor.b * mixcolor.a ? newB6 : mixcolor.b * mixcolor.a;
        }
        //MARK: SOFT LIGHT FILTER
        else if(filter_type == 8){
            newR7 = mixcolor.r * mixcolor.a < 0.5 ? (2.0 * newR6 * mixcolor.r * mixcolor.a + newR6 * newR6 * (1.0 - 2.0 * mixcolor.r * mixcolor.a)) : (sqrt(newR6) * (2.0 * mixcolor.r * mixcolor.a - 1.0) + (2.0 * newR6) * (1.0 - mixcolor.r * mixcolor.a));
            newG7 = mixcolor.g * mixcolor.a < 0.5 ? (2.0 * newG6 * mixcolor.g * mixcolor.a + newG6 * newG6 * (1.0 - 2.0 * mixcolor.g * mixcolor.a)) : (sqrt(newG6) * (2.0 * mixcolor.g * mixcolor.a - 1.0) + (2.0 * newG6) * (1.0 - mixcolor.g * mixcolor.a));
            newB7 = mixcolor.b * mixcolor.a < 0.5 ? (2.0 * newB6 * mixcolor.b * mixcolor.a + newB6 * newB6 * (1.0 - 2.0 * mixcolor.b * mixcolor.a)) : (sqrt(newB6) * (2.0 * mixcolor.b * mixcolor.a - 1.0) + (2.0 * newB6) * (1.0 - mixcolor.b * mixcolor.a));
        }
        //MARK: BURN FILTER
        else if(filter_type == 9){
            newR7 = 1.0 - (1.0 - newR6) / (mixcolor.r * mixcolor.a);
            newG7 = 1.0 - (1.0 - newG6) / (mixcolor.g * mixcolor.a);
            newB7 = 1.0 - (1.0 - newB6) / (mixcolor.b * mixcolor.a);
        }
        //MARK: LIGHTEN FILTER
        else if(filter_type == 10){
            newR7 = newR6 >= mixcolor.r * mixcolor.a ? newR6 : mixcolor.r * mixcolor.a;
            newG7 = newG6 >= mixcolor.g * mixcolor.a ? newG6 : mixcolor.g * mixcolor.a;
            newB7 = newB6 >= mixcolor.b * mixcolor.a ? newB6 : mixcolor.b * mixcolor.a;
        }
        //MARK: HARD LIGHT FILTER
        else if(filter_type == 11){
            newR7 = mixcolor.r * mixcolor.a < 0.5 ? (2.0 * newR6 * mixcolor.r * mixcolor.a) : (1.0 - 2.0 * (1.0 - newR6) * (1.0 - mixcolor.r * mixcolor.a));
            newG7 = mixcolor.g * mixcolor.a < 0.5 ? (2.0 * newG6 * mixcolor.g * mixcolor.a) : (1.0 - 2.0 * (1.0 - newG6) * (1.0 - mixcolor.g * mixcolor.a));
            newB7 = mixcolor.b * mixcolor.a < 0.5 ? (2.0 * newB6 * mixcolor.b * mixcolor.a) : (1.0 - 2.0 * (1.0 - newB6) * (1.0 - mixcolor.b * mixcolor.a));
        }
        //MARK: LINEAR BURN FILTER
        else if(filter_type == 12){
            newR7 = newR6 + mixcolor.r * mixcolor.a - 1.0;
            newG7 = newG6 + mixcolor.g * mixcolor.a - 1.0;
            newB7 = newB6 + mixcolor.b * mixcolor.a - 1.0;
        }
        //MARK: ADDITION FILTER
        else if(filter_type == 13){
            newR7 = newR6 + mixcolor.r * mixcolor.a <= 1.0 ? newR6 + mixcolor.r * mixcolor.a : 1.0;
            newG7 = newG6 + mixcolor.g * mixcolor.a <= 1.0 ? newG6 + mixcolor.g * mixcolor.a : 1.0;
            newB7 = newB6 + mixcolor.b * mixcolor.a <= 1.0 ? newB6 + mixcolor.b * mixcolor.a : 1.0;
        }
        //MARK: DIVIDE FILTER
        else if(filter_type == 14){
            newR7 = newR6 / (mixcolor.r * mixcolor.a);
            newG7 = newG6 / (mixcolor.g * mixcolor.a);
            newB7 = newB6 / (mixcolor.b * mixcolor.a);
        }
        //MARK: SUBSTRACT FILTER
        else if(filter_type == 15){
            newR7 = newR6 - mixcolor.r * mixcolor.a >= 0.0 ? newR6 - mixcolor.r * mixcolor.a : 0.0;
            newG7 = newG6 - mixcolor.g * mixcolor.a >= 0.0 ? newG6 - mixcolor.g * mixcolor.a : 0.0;
            newB7 = newB6 - mixcolor.b * mixcolor.a >= 0.0 ? newB6 - mixcolor.b * mixcolor.a : 0.0;
        }
        //MARK: VIVID LIGHT FILTER
        else{
            newR7 = mixcolor.r * mixcolor.a <= 0.5 ? newR6 / (1.0 - mixcolor.r * mixcolor.a) : 1.0 - (1.0 - newR6) / (mixcolor.r * mixcolor.a);
            newG7 = mixcolor.g * mixcolor.a <= 0.5 ? newG6 / (1.0 - mixcolor.g * mixcolor.a) : 1.0 - (1.0 - newG6) / (mixcolor.g * mixcolor.a);
            newB7 = mixcolor.b * mixcolor.a <= 0.5 ? newB6 / (1.0 - mixcolor.b * mixcolor.a) : 1.0 - (1.0 - newB6) / (mixcolor.b * mixcolor.a);
        }
        
        float3 mixedValues = mix(float3(newR6, newG6, newB6), float3(newR7, newG7, newB7), filter_intensity);
        
        float3 linearValues = srgb_to_linear(float3(mixedValues.r, mixedValues.g, mixedValues.b));
        
        float3 clampedValues = clamp(linearValues, float3(0,0,0), float3(5,5,5));

        return float4(clampedValues.r, clampedValues.g, clampedValues.b, 1.0);
    }
    
}}



//
//  MenuCollectionViewCell.swift
//  MakeYourFilter
//
//  Created by Victoria on 6/29/19.
//  Copyright © 2019 Delta Squad. All rights reserved.
//

import UIKit

class MenuCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet var circleView: UIView!
    
    @IBOutlet var menuIcon: UIImageView!
    
    @IBOutlet var menuLabel: UILabel!
    
    @IBOutlet var singleLabel: UILabel!
    @IBOutlet var singleIcon: UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        
    }
    
    func setCircleView() {
        circleView.layer.cornerRadius = circleView.frame.height / 2
        circleView.layer.borderWidth = 1.0
        circleView.layer.borderColor = #colorLiteral(red: 0.4980392157, green: 0.4980392157, blue: 0.4980392157, alpha: 1)
    }
    
    func setViewWithImage() {
        menuIcon.isHidden = true
        menuLabel.isHidden = true
        singleLabel.isHidden = true
        singleIcon.isHidden = false
    }
    
    func setViewWithLabel(){
        menuIcon.isHidden = true
        menuLabel.isHidden = true
        singleLabel.isHidden = false
        singleIcon.isHidden = true
    }
    
    func setViewWithLabelImage(){
        menuIcon.isHidden = false
        menuLabel.isHidden = false
        singleLabel.isHidden = true
        singleIcon.isHidden = true
    }
    
    
    
}

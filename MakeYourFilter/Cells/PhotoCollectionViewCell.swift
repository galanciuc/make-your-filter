//
//  PhotoCollectionViewCell.swift
//  MakeYourFilter
//
//  Created by Victoria on 6/22/19.
//  Copyright © 2019 Delta Squad. All rights reserved.
//

import UIKit

class PhotoCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet var photoImageView: UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
    }

}

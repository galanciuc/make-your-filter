//
//  MakeYourFilterCollectionViewCell.swift
//  MakeYourFilter
//
//  Created by Victoria on 6/29/19.
//  Copyright © 2019 Delta Squad. All rights reserved.
//

import UIKit

class FilterCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet var imageView: UIImageView!
    @IBOutlet var label: UILabel!
    @IBOutlet var activityIndicator: UIActivityIndicatorView!
    @IBOutlet var editView: UIView!
    @IBOutlet var backgroundImage: UIView!
    
    override func layoutSubviews() {
        super.layoutSubviews()
    }
    
    func setImageCorner() {
        imageView.layer.cornerRadius = imageView.frame.height / 2
        backgroundImage.layer.cornerRadius = backgroundImage.frame.height / 2
    }
    
    func setLabel(with: String) {
        label.text = with
    }
    
}

//
//  MakeYourFilterCollectionViewCell.swift
//  MakeYourFilter
//
//  Created by Victoria on 6/29/19.
//  Copyright © 2019 Delta Squad. All rights reserved.
//

import UIKit

class MakeYourFilterCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet var imageView: UIImageView!
    @IBOutlet var label: UILabel!
    @IBOutlet var activityIndicator: UIActivityIndicatorView!
    
    override func layoutSubviews() {
        super.layoutSubviews()
    }
    
    func setImageCorner() {
        imageView.layer.cornerRadius = imageView.frame.height / 2
    }
    
    func setLabel(with: String) {
        label.text = with
    }
    
}

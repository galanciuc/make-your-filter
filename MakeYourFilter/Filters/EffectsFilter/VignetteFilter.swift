//
//  VignetteFilter.swift
//  MakeYourFilter
//
//  Created by Tolea on 1/27/20.
//  Copyright © 2020 Delta Squad. All rights reserved.
//

import Foundation
import CoreImage

class VignetteFilter: CIFilter {
    
    private let kernel: CIKernel;
    
    private let callback: CIKernelROICallback;
    
    var inputImage: CIImage?
    
    override init() {
        let url = Bundle.main.url(forResource: "default", withExtension: "metallib")!
        let data = try! Data(contentsOf: url);
        kernel = try! CIKernel(functionName: "vignetteShader", fromMetalLibraryData: data)
        
        callback = {index, rect in
            return rect;
        }
        
        super.init()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func outputVignette(vignette: Float, vignettePower: Float) -> CIImage? {
        
        guard let inputImage = inputImage else{return nil}
        
        return kernel.apply(extent: inputImage.extent, roiCallback: callback, arguments: [inputImage, vignette, vignettePower]);
        
    }
    
}

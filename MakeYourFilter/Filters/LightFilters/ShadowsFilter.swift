//
//  ShadowsFilter.swift
//  MakeYourFilter
//
//  Created by Tolea on 1/27/20.
//  Copyright © 2020 Delta Squad. All rights reserved.
//

import Foundation
import CoreImage

class ShadowsFilter: CIFilter {
    
    private let kernel: CIColorKernel;
    
    var inputImage: CIImage?
    
    override init() {
        let url = Bundle.main.url(forResource: "default", withExtension: "metallib")!
        let data = try! Data(contentsOf: url);
        kernel = try! CIColorKernel(functionName: "shadowsHighlightsShader", fromMetalLibraryData: data)
        
        super.init()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func outputShadows(shadows: Float, highlights: Float) -> CIImage? {
        
        guard let inputImage = inputImage else{return nil}
        
        return kernel.apply(extent: inputImage.extent, arguments: [inputImage, shadows, highlights]);
        
    }
    
}

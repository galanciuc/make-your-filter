//
//  MetalFilter.swift
//  MakeYourFilter
//
//  Created by Tolea on 1/24/20.
//  Copyright © 2020 Delta Squad. All rights reserved.
//

import Foundation
import CoreImage

class MetalFilter: CIFilter {
    
    private let kernel: CIColorKernel;
    
    var inputImage: CIImage?
    
    override init() {
        let url = Bundle.main.url(forResource: "default", withExtension: "metallib")!
        let data = try! Data(contentsOf: url)
        kernel = try! CIColorKernel(functionName: "editShader", fromMetalLibraryData: data)
        
        super.init()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func outputImage(filter_type: Int, selectedR: Float, selectedG: Float, selectedB: Float, selectedA: Float, contrast: Float, brightness: Float, shadows: Float, highlights: Float, lux: Float, temperature: Float, tint: Float, saturation: Float, vibrance: Float, gamma: Float, hue: Float, posterize: Float, intensity: Float) -> CIImage? {
        guard let inputImage = inputImage else {return nil}
        
        return kernel.apply(extent: inputImage.extent, arguments: [inputImage, Float(filter_type), selectedR, selectedG, selectedB, selectedA, contrast, brightness, shadows, highlights, lux, temperature, tint, saturation, vibrance, gamma, hue, posterize, intensity]);
        
    }
    
}

//
//  TemperatureFilter.swift
//  MakeYourFilter
//
//  Created by Tolea on 1/27/20.
//  Copyright © 2020 Delta Squad. All rights reserved.
//

import Foundation
import CoreImage

class TemperatureFilter: CIFilter {
    
    private let kernel: CIColorKernel;
    
    var inputImage: CIImage?
    
    override init() {
        let url = Bundle.main.url(forResource: "default", withExtension: "metallib")!
        let data = try! Data(contentsOf: url);
        kernel = try! CIColorKernel(functionName: "temperatureTintShader", fromMetalLibraryData: data)
        
        super.init()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func outputTemperature(temperature: Float, tint: Float) -> CIImage? {
        
        guard let inputImage = inputImage else{return nil}
        
        return kernel.apply(extent: inputImage.extent, arguments: [inputImage, temperature, tint]);
        
    }
    
}

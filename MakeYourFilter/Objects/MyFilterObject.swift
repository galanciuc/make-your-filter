//
//  MyFilterObject.swift
//  MakeYourFilter
//
//  Created by Tolea on 2/18/20.
//  Copyright © 2020 Delta Squad. All rights reserved.
//

import Foundation
import UIKit

class MyFilterObject: NSObject, NSCoding {
    var filter_name : String = "";
    
    var filter_type : Int = 0;
    
    var filter_r : Float = 0.0;
    var filter_g : Float = 0.0;
    var filter_b : Float = 0.0;
    var filter_opacity : Float = 0.0
    
    var filter_image: UIImage?
    var filter_image_loaded: Bool = false
    
    init(name : String, type : Int, red : Float, green : Float, blue : Float, opacity : Float) {
        
        self.filter_name = name;
        self.filter_type = type;
        self.filter_r = red;
        self.filter_g = green;
        self.filter_b = blue;
        self.filter_opacity = opacity;
        
    }
    
    required convenience init(coder aDecoder: NSCoder) {
        let filter_name = aDecoder.decodeObject(forKey: "filter_name") as! String
        let filter_type = aDecoder.decodeInteger(forKey: "filter_type")
        let filter_r = aDecoder.decodeFloat(forKey: "filter_r")
        let filter_g = aDecoder.decodeFloat(forKey: "filter_g")
        let filter_b = aDecoder.decodeFloat(forKey: "filter_b")
        let filter_opacity = aDecoder.decodeFloat(forKey: "filter_opacity")
        
        self.init(name : filter_name, type : filter_type, red : filter_r, green : filter_g, blue : filter_b, opacity : filter_opacity)
    }
    
    func encode(with coder: NSCoder) {
        coder.encode(filter_name, forKey: "filter_name")
        coder.encode(filter_type, forKey: "filter_type")
        coder.encode(filter_r, forKey: "filter_r")
        coder.encode(filter_g, forKey: "filter_g")
        coder.encode(filter_b, forKey: "filter_b")
        coder.encode(filter_opacity, forKey: "filter_opacity")
    }
    
    func setImage(image: UIImage) {
        filter_image = image
        filter_image_loaded = true
    }
}

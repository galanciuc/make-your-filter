//
//  FilterObject.swift
//  MakeYourFilter
//
//  Created by Tolea on 1/15/20.
//  Copyright © 2020 Delta Squad. All rights reserved.
//

import Foundation
import UIKit

class FilterObject {
    
    var filter_name : String = ""
    
    var filter_type : Int = 0
    
    var filter_r : Float = 0.0
    var filter_g : Float = 0.0
    var filter_b : Float = 0.0
    var filter_opacity : Float = 0.0
    var filter_intensity: Float = 1.0
    
    var filter_image: UIImage?
    var filter_image_loaded: Bool = false
    
    init(name : String, type : Int, red : Float, green : Float, blue : Float, opacity : Float, intensity: Float) {
        
        self.filter_name = name;
        self.filter_type = type;
        self.filter_r = red;
        self.filter_g = green;
        self.filter_b = blue;
        self.filter_opacity = opacity;
        self.filter_intensity = intensity
        
    }
    
    func setImage(image: UIImage) {
        filter_image = image
        filter_image_loaded = true
    }
    
}

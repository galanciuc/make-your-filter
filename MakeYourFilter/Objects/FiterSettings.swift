//
//  FiterSettings.swift
//  MakeYourFilter
//
//  Created by Tolea on 1/10/20.
//  Copyright © 2020 Delta Squad. All rights reserved.
//

import Foundation

class FilterSettings{
    var selectedFilterType = 0;
    var selectedR : Float = 0.0;
    var selectedG : Float = 0.0;
    var selectedB : Float = 0.0;
    var selectedAlpha : Float = 0.0;
    var selectedIntensity : Float = 0.0;
    
    var selectedBrightness : Float = 0.0;
    var selectedContrast : Float = 1.0;
    var selectedShadow : Float = 0.0;
    var selectedHighlight : Float = 0.0;
    var selectedLux : Float = 0.0;
    
    var selectedTemperature : Float = 0.0;
    var selectedTint : Float = 0.0;
    var selectedSaturation : Float = 1.0;
    var selectedVibrance : Float = 0.0;
    var selectedGamma : Float  = 1.0
    var selectedHue : Float = 0.0;
    
    var selectedPosterize : Float = 25.0;
    var selectedVignette : Float = 1.1;
    var selectedVignettePower : Float = 0.0;
    var blurPower : Float = 0.0;
    
    var selectedFilter : Int = -1;
    
    init(filter_type: Int, r: Float, g: Float, b: Float, alpha: Float, brightness: Float, contrast: Float, shadow: Float, highlight: Float, lux: Float, temperature: Float, tint: Float, saturation: Float, vibrance: Float, gamma: Float, hue: Float, posterize: Float, vignette: Float, vignette_power: Float, blur_power: Float, intensity: Float, selected_filter: Int) {
        
        self.selectedFilterType = filter_type;
        self.selectedR = r;
        self.selectedG = g;
        self.selectedB = b;
        self.selectedAlpha = alpha;
        self.selectedIntensity = intensity;
        self.selectedBrightness = brightness;
        self.selectedContrast = contrast;
        self.selectedShadow = shadow;
        self.selectedHighlight = highlight;
        self.selectedLux = lux;
        self.selectedTemperature = temperature;
        self.selectedTint = tint;
        self.selectedSaturation = saturation;
        self.selectedVibrance = vibrance;
        self.selectedGamma = gamma;
        self.selectedHue = hue;
        self.selectedPosterize = posterize;
        self.selectedVignette = vignette;
        self.selectedVignettePower = vignette_power;
        self.blurPower = blur_power;
        self.selectedFilter = selected_filter
        
    }
    
}
